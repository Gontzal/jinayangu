/* 
-------------------------------------------------------------------------
- Hasiera data: 2015ko otsailean                                         -
-
 <JinaYangu- Programa para trabajar la lectura y escritura>
    Copyright (C) <año>  <nombre del autor>

    Este programa es software libre: usted puede redistribuirlo y/o modificarlo 
    bajo los términos de la Licencia Pública General GNU publicada 
    por la Fundación para el Software Libre, ya sea la versión 3 
    de la Licencia, o (a su elección) cualquier versión posterior.

    Este programa se distribuye con la esperanza de que sea útil, pero 
    SIN GARANTÍA ALGUNA; ni siquiera la garantía implícita 
    MERCANTIL o de APTITUD PARA UN PROPÓSITO DETERMINADO. 
    Consulte los detalles de la Licencia Pública General GNU para obtener 
    una información más detallada. 
-------------------------------------------------------------------------
*/


uses Cairo
uses Gtk
	
class Cairo_interface:Gtk.Window
	boton_ejercicio : list of Texto
	boton_tipo : list of Imagen
	boton_idioma: Imagen
	box:Gtk.Paned
	lienzo:DrawingArea
	caja_eventos:EventBox
	event lienzo_izq_pulsado()
	event lienzo_der_pulsado()
	event lienzo_soltado()
	arrastrando:bool
	guardado:bool
	control_seleccionado:int
	control_tomado:int
	lienzo_ancho:int
	lienzo_alto:int
	timer:uint
	boton:Button
	controles: list of Control // La lista de controles esta definida en Katamotz_user_interface
	referencia_general:int=0
	tomado: bool=false
	tomado_ontop:bool=false
	tomado_x:int
	tomado_y:int
	init 
		controles= new list of Control // La lista de controles esta definida en Katamotz_user_interface
		arrastrando=false
		control_seleccionado=-1
		lienzo_ancho=600
		lienzo_alto=600
	
	def on_toma (evento:Gdk.EventButton):bool // evento cuando se pulsa un boton del mouse
		if evento.button==1
			this.lienzo_izq_pulsado ()
			for var i=0 to ultimo_de_lista(controles)
				if controles[i].visible
					var romper_bucle=false
					case controles[i].tipo
						when "Imagen"
							if colision_xy_cuadrada ((int)evento.x,(int)evento.y,controles[i])
								if not is_alpha ((int)evento.x-controles[i].x,(int)evento.y-controles[i].y,controles[i].pixbufer)
									if not controles[i].figurafondo
										controles.insert (0,controles[i])
										control_seleccionado=0
										control_tomado=0
									else
										control_tomado=i
										control_seleccionado=i
									controles[control_tomado].izq_pulsado(controles[control_tomado])
									controles[control_tomado].tomado_x=(int)evento.x-controles[control_tomado].x
									controles[control_tomado].tomado_y=(int)evento.y-controles[control_tomado].y
									if controles[control_tomado].arrastrable do arrastrando=true
									romper_bucle=true
						when "Texto","Rectangulo","Entrada"
							if colision_xy_cuadrada ((int)evento.x,(int)evento.y,controles[i])
								if not controles[i].figurafondo
									controles.insert (0,controles[i])
									control_seleccionado=0
									control_tomado=0
								else
									control_tomado=i
									control_seleccionado=i
									
								controles[control_tomado].izq_pulsado(controles[control_tomado])
								controles[control_tomado].tomado_x=(int)evento.x-controles[control_tomado].x
								controles[control_tomado].tomado_y=(int)evento.y-controles[control_tomado].y
								if controles[control_tomado].arrastrable do arrastrando=true
								romper_bucle=true
								
					if romper_bucle do break
		else if evento.button==3
			this.lienzo_der_pulsado ()
			for var i=0 to ultimo_de_lista(controles)
				var romper_bucle=false
				case controles[i].tipo
					when "Imagen","Texto","Rectangulo"
						if colision_xy_cuadrada ((int)evento.x,(int)evento.y,controles[i])
							controles[i].der_pulsado(controles[i])
							romper_bucle=true
				if romper_bucle do break
			
		lienzo.queue_draw_area(0,0,lienzo_ancho,lienzo_alto) //Refresca la pantalla
		return true
		
		
	def on_deja (evento:Gdk.EventButton):bool // evento cuando se suelta un boton del mouse
		arrastrando=false
		
		if control_tomado>=0 do controles[control_tomado].soltado(controles[control_tomado])
		control_tomado=-1
		
		lienzo.queue_draw_area(0,0,lienzo_ancho,lienzo_alto) //Refresca la pantalla
		
		return true
		
	def on_mover (evento:Gdk.EventMotion):bool // evento cuando se mueve el mouse
		if arrastrando 
			if evento.x>5 and evento.x<lienzo_ancho-5 and evento.y>5 and evento.y<lienzo_alto-5
				case controles[control_tomado].tipo
					when "Imagen","Texto","Rectangulo"
					if controles[control_tomado].arrastrable
						controles[control_tomado].x=(int)evento.x-controles[control_tomado].tomado_x
						controles[control_tomado].y=(int)evento.y-controles[control_tomado].tomado_y
						
			pintar()
		return true
	def pintar()
		lienzo.queue_draw() //Refresca la pantalla
	def on_pinta(e:Gtk.Widget,ctx:Context) : bool // funcion para pintar el escenario
		lienzo.grab_focus()
		//dibujando todos los controles desde el último hasta el primero
		var i= ultimo_de_lista(controles)
		while i>=0
			// le advertimos al control si esta seleccionado o no
			if i!=control_seleccionado
				controles[i].seleccionado=false
			else
				controles[i].seleccionado=true
			// pintamos el control
			controles[i].pinta(ctx)// pintando un control
			// si esta seleccionado o tomado entonces 
			i--
			
		return true

	def on_tecla(evento:Gdk.EventKey):bool
		if control_seleccionado!=-1
			print control_seleccionado.to_string()
			if controles[control_seleccionado].tipo=="Entrada"
				controles[control_seleccionado].gestiona_tecla(evento.keyval,evento.str)
				print "Tecla dentro"
		print "Tecla fuera"
		lienzo.queue_draw() //Refresca la pantalla
		return true
	
		
	def inicializa_pantalla()
		
		// pon la ventana en la medida
		this.set_size_request(lienzo_ancho+40,lienzo_alto)
		//this.configure_event.connect(recalcula_cuadro)
		// lienzo es la zona da dibujo
		lienzo = new DrawingArea()
		lienzo.draw.connect(on_pinta)
		lienzo.queue_draw()
		// fondo de pantalla
		var color=Gdk.RGBA(); color.red=0.2; color.green=0; color.blue=0.9; color.alpha=1;
		this.override_background_color(StateFlags.NORMAL,color)
		
		// introducimos lienzo (zona de dibujo) en una caja de eventos para recoger los eventos del raton.
		caja_eventos= new EventBox ()
		caja_eventos.add_events(Gdk.EventMask.BUTTON_PRESS_MASK)
		caja_eventos.button_press_event.connect(on_toma)
		
		caja_eventos.add_events(Gdk.EventMask.BUTTON_RELEASE_MASK)
		caja_eventos.button_release_event.connect(on_deja)
		
		caja_eventos.add_events(Gdk.EventMask.POINTER_MOTION_MASK)
		caja_eventos.motion_notify_event.connect(on_mover)
		
		caja_eventos.add_events(Gdk.EventMask.KEY_PRESS_MASK)
		caja_eventos.key_press_event.connect(on_tecla)
		
		lienzo.set_can_focus(true)
		lienzo.grab_focus()
		caja_eventos.add(lienzo)

		box=new Gtk.Paned (Gtk.Orientation.HORIZONTAL);
		box.set_position(40)
		box.add(caja_eventos)
		this.add(box)
		
		lienzo.show_all()
		caja_eventos.show_all()
		box.show_all()
		this.show_all()

	def inicio(ancho:int,alto:int)
		this.lienzo_ancho=ancho
		this.lienzo_alto=alto
		cargar_escenario()
		inicializa_pantalla()
		this.show()
		this.set_size_request(lienzo_ancho,lienzo_alto)
				
	def cargar_escenario()
		controles.clear()

	
