class Crear_nombre:Gtk.Window
	directorio_nombre:string
	entrada_nombre:Gtk.Entry
	entradas:list of Gtk.Entry
	boton_imagen: Gtk.Button
	init
		destroy.connect(on_salir)
		
	def inicio()
		directorio_nombre=crear_directorio_usuario_tmp();
		this.show_all()
		this.set_size_request(700,500)
		var fixed= new Gtk.Fixed()
		this.add(fixed)
		
		// primera linea Nombre
		fixed.put(new Gtk.Label("Nombre:"),15,10)
		entrada_nombre= new Gtk.Entry ()
		fixed.put (entrada_nombre,100,10)
		var img = new Gtk.Image.from_file (directorio_datos+"/imagenes/erabilgarri/micro20.png");		
		var graba_nombre = new Gtk.Button()
		graba_nombre.set_image(img)
		fixed.put (graba_nombre,300,5)
		graba_nombre.clicked.connect(on_graba_nombre)
		
		var img2 = new Gtk.Image.from_file (directorio_datos+"/imagenes/erabilgarri/ok20.png");		
		var stop_nombre = new Gtk.Button()
		stop_nombre.set_image(img2)
		fixed.put (stop_nombre,340,5)
		stop_nombre.clicked.connect(on_stop_nombre)
		
		var img3 = new Gtk.Image.from_file (directorio_datos+"/imagenes/erabilgarri/play20.png");		
		var play_nombre = new Gtk.Button()
		play_nombre.set_image(img3)
		fixed.put (play_nombre,380,5)
		play_nombre.clicked.connect(on_play_nombre)
		
		// Silabas
		entradas= new list of Gtk.Entry
		var grabadoras= new list of Gtk.Button
		var plays= new list of Gtk.Button
		var stops= new list of Gtk.Button
		for var i=0 to 5
			fixed.put(new Gtk.Label("Sílaba"+" "+(i+1).to_string()),50,65+40*i)
			entradas.add(new Gtk.Entry ())
			entradas.last().set_width_chars(5)
			fixed.put (entradas.last(),120,60+40*i)
			
			var imgn1 = new Gtk.Image.from_file (directorio_datos+"/imagenes/erabilgarri/micro20.png");		
			grabadoras.add(new Gtk.Button())
			grabadoras.last().set_image(imgn1)
			grabadoras.last().set_name((i+1).to_string())
			fixed.put (grabadoras.last(),200,55+40*i)
			grabadoras.last().clicked.connect(on_grabadoras)
			
			var imgn2 = new Gtk.Image.from_file (directorio_datos+"/imagenes/erabilgarri/ok20.png");		
			stops.add (new Gtk.Button())
			stops.last().set_image(imgn2)
			stops.last().set_name((i+1).to_string())
			fixed.put (stops.last(),240,55+40*i)
			stops.last().clicked.connect(on_stops)
			
			var imgn3 = new Gtk.Image.from_file (directorio_datos+"/imagenes/erabilgarri/play20.png");		
			plays.add (new Gtk.Button())
			plays.last().set_name((i+1).to_string())
			plays.last().set_image(imgn3)
			fixed.put (plays.last(),280,55+40*i)
			plays.last().clicked.connect(on_plays)
			
		// dibujo
		fixed.put(new Gtk.Label("Imagen:"),400,60)
		var imagen = new Gtk.Image.from_file (directorio_datos+"/imagenes/erabilgarri/dibujo.jpg");
		copia_archivo(directorio_datos+"/imagenes/erabilgarri/dibujo.jpg", directorio_nombre+"/imagen")
		boton_imagen= new Gtk.Button()
		boton_imagen.set_image(imagen)
		fixed.put(boton_imagen,400,90)
		boton_imagen.clicked.connect(on_imagen)

		
		var boton_guardar= new Gtk.Button.with_label("Guardar")
		boton_guardar.clicked.connect(on_guardar)
		fixed.put(boton_guardar,10,400)

		var boton_abrir= new Gtk.Button.with_label("Abrir")
		boton_abrir.clicked.connect(on_abrir)
		fixed.put(boton_abrir,110,400)
		
		this.show_all()
		
	def on_salir()
		
		borra_directorio_interior(directorio_nombre)
		borra_directorio_vacio(directorio_nombre)
	
	
	def crear_directorio_usuario_tmp():string
		var r=""
		r=directorio_usuario+"/."+crea_nombre()
		crea_directorio(r)
		return r
	
	def on_play_nombre()
		stop_aplay()
		stop_arecord()
		aplay(directorio_nombre+"/0")
		
	def on_stop_nombre()
		stop_aplay()
		stop_arecord()
		
	def on_graba_nombre()
		stop_aplay()
		stop_arecord()
		arecord(directorio_nombre+"/0")	
		
	def on_grabadoras(w:Gtk.Widget)
		var num= w.get_name()
		stop_aplay()
		stop_arecord()
		arecord(directorio_nombre+"/"+num)
		
		
	def on_plays(w:Gtk.Widget)
		var num= w.get_name()
		stop_aplay()
		stop_arecord()
		aplay(directorio_nombre+"/"+num)
		
		
	def on_stops()
		stop_aplay()
		stop_arecord()
		
	
		
	def on_imagen()
		var FC=  new Gtk.FileChooserDialog (("Insertar imagen:"), this, Gtk.FileChooserAction.OPEN,
			t.t("_Abrir"),Gtk.ResponseType.ACCEPT,
			t.t("_Cerrar"),Gtk.ResponseType.CANCEL);
			
		FC.select_multiple = false;
		FC.set_modal(true)
		case FC.run ()
			when Gtk.ResponseType.CANCEL
				FC.hide()
				FC.close()
			when Gtk.ResponseType.ACCEPT
				FC.hide()
				var nombre_archivo=FC.get_filename ();
				borra_archivo(directorio_nombre+"/imagen")
				copia_archivo(nombre_archivo , directorio_nombre+"/imagen")
				try
					var pixbufer_original=new Gdk.Pixbuf.from_file(directorio_nombre+"/imagen")
					var pixbufer=pixbufer_original.scale_simple(150,150, Gdk.InterpType.BILINEAR)	
					var imagen = new Gtk.Image.from_pixbuf (pixbufer);		
					boton_imagen.set_image(imagen)
				except
					borra_archivo(directorio_nombre+"/imagen")
					copia_archivo(directorio_datos+"/imagenes/erabilgarri/dibujo.jpg", directorio_nombre+"/imagen")
				
		
	def on_guardar()
		// crea el archivo de informacion de los nombres.
		crea_archivo_escribiendo(directorio_nombre+"/nombre",
			entrada_nombre.get_text()+
			"\n"+entradas[0].get_text()+
			"\n"+entradas[1].get_text()+
			"\n"+entradas[2].get_text()+
			"\n"+entradas[3].get_text()+
			"\n"+entradas[4].get_text()+
			"\n"+entradas[5].get_text())
		
		GLib.Environment.set_current_dir (directorio_usuario+"/nombres")
		var FC=  new Gtk.FileChooserDialog (t.t("Crear nombre:"), this, Gtk.FileChooserAction.SAVE,
		t.t("_Guardar"),Gtk.ResponseType.ACCEPT,
		t.t("_Cerrar"),Gtk.ResponseType.CANCEL);
		FC.select_multiple = false;
		FC.set_modal(true)
		case FC.run ()
			when Gtk.ResponseType.CANCEL
				FC.hide()
				FC.close()
			when Gtk.ResponseType.ACCEPT
				FC.hide()
				var nombre_archivo=FC.get_filename()

				// Copiando archivos y revisando errores
				if not existe_archivo(nombre_archivo+".kjn") //si no existe el archivo entonces va bien.
					// comprime el directorio
					comprime_directorio(directorio_nombre,nombre_archivo+".kjn")
		
					datos.abrir_directorio_nombres()
				else
					var m= new Gtk.MessageDialog (this, Gtk.DialogFlags.MODAL, Gtk.MessageType.WARNING, Gtk.ButtonsType.OK_CANCEL, 
						t.t("Nombre de archivo ya existe. No pudo ser guardado."));
					m.run()
					m.close()
				
		
		
	def on_abrir()
		GLib.Environment.set_current_dir (directorio_usuario+"/nombres")
		var FC=  new Gtk.FileChooserDialog (t.t("Abrir nombre:"), this, Gtk.FileChooserAction.SAVE,
		t.t("_Abrir"),Gtk.ResponseType.ACCEPT,
		t.t("_Cerrar"),Gtk.ResponseType.CANCEL);
		FC.select_multiple = false;
		FC.set_modal(true)
		case FC.run ()
			when Gtk.ResponseType.CANCEL
				FC.hide()
				FC.close()
			when Gtk.ResponseType.ACCEPT
				FC.hide()
				var nombre_archivo=FC.get_filename()
				directorio_nombre=crear_directorio_usuario_tmp();
				descomprime_archivo(nombre_archivo,directorio_nombre)
				var lista=datos.abrir_archivo_nombre(directorio_nombre)
				try
					var imagen = new Gtk.Image.from_pixbuf (new Gdk.Pixbuf.from_file(directorio_nombre+"/imagen").scale_simple(150,150, Gdk.InterpType.BILINEAR));
					boton_imagen.set_image(imagen)
				except
					pass
				
				entrada_nombre.set_text(lista[0])
				for var i=1 to 6 do entradas[i-1].set_text("")
				for var i=1 to 6 do entradas[i-1].set_text(lista[i])
				
