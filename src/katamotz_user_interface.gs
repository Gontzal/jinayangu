uses Cairo
uses Pango



def Mm (s:string):string
	var r=s
	if mayusculas do r=r.up()
	return r

blanco:string; negro:string; amarillo:string; verde:string; rojo:string; azul:string; transparente:string;		
def inicializa_colores()
	blanco="1#1#1"; negro="0#0#0"; amarillo="1#1#0"; verde="0#1#0"; rojo="1#0#0"; azul="0#0#1";
	transparente="0#0#0#0"

def get_tamano_de_texto(t:string,family:string,tamano:double):int
	surface:Cairo.Surface= new Cairo.ImageSurface(Cairo.Format.ARGB32,40,70)
	var cr= new Cairo.Context(surface)
	var font= new Pango.FontDescription()
	font.set_family(family)
	font.set_size((int)(tamano * Pango.SCALE));
	var l= cairo_create_layout(cr)
	l.set_text(t,300)
	l.set_font_description(font)
	var a=0
	l.get_pixel_size(out a, null)
	a=a-a/4
	return a

def is_alpha (x:int,y:int,imagen:Gdk.Pixbuf):bool
	var canales= imagen.get_n_channels()
	var pixfila= imagen.get_rowstride()	
	pixel:uint8*
	pixel=imagen.get_pixels()
	pixel+= 3+(y * pixfila)+ (x*canales) 	
	var r=false
	if *pixel<1 do r=true
	//print r.to_string()
	return r


def colision_cuadrada (a:Control,b:Control):bool
	var res=false
	//if (a.Index!=b.Index) and (a.Visible) and (b.Visible)
	/*a - bottom right co-ordinates*/
	var ax= a.get_posx()
	var ay= a.get_posy()
	var ax1 = ax + a.get_ancho();
	var ay1 = ay + a.get_alto();
	
	/*b - bottom right co-ordinates*/
	var bx= b.get_posx()
	var by= b.get_posy()
	var bx1 = bx + b.get_ancho();
	var by1 = by + b.get_alto();
	
	/*check if bounding boxes intersect*/
	var rectcolision=true
	if((bx1 < ax) or (ax1 < bx)) do rectcolision=false
	if((by1 < ay) or (ay1 < by)) do rectcolision=false
	res=rectcolision
	return res
		
		

def colision_xy_cuadrada (x:int,y:int,a:Control):bool
	
	var res=false
	//if (a.Index!=b.Index) and (a.Visible) and (b.Visible)
	/*a - bottom right co-ordinates*/
	var ax= a.get_posx()
	var ay= a.get_posy()
	var ax1 = ax + a.get_ancho();
	var ay1 = ay + a.get_alto();
	
	
	/*check if bounding boxes intersect*/
	var rectcolision=true
	if((x < ax) or (x > ax1)) do rectcolision=false
	if((y < ay) or (y > ay1)) do rectcolision=false
	res=rectcolision
	return res


	
def colision_mascara(a:Control,b:Control):bool
	//no funciona si las imagenes han sido ampliadas o reducidas.
	var res=false
	if (a.visible) and (b.visible)
		/*a - bottom right co-ordinates*/
		var ax=a.get_posx()
		var ay=a.get_posy()
		var ax1 = a.get_posx() + a.get_ancho() - 1;
		var ay1 = a.get_posy() + a.get_alto() - 1;
		
		/*b - bottom right co-ordinates*/
		var bx=b.get_posx()
		var by=b.get_posy()
		var bx1 = b.get_posx() + b.get_ancho() - 1;
		var by1 = b.get_posy() + b.get_alto() - 1;

		/*check if bounding boxes intersect*/
		var rectcolision=true
		if((bx1 < ax) or (ax1 < bx)) do rectcolision=false
		if((by1 < ay) or (ay1 < by)) do rectcolision=false
		
		if rectcolision
			//recoge el cuadrado de interseccion
			var x0 = int.max(ax,bx)
			var y0 = int.max(ay,by)
			var x1 = int.min(ax1,bx1)
			var y1 = int.min(ay1,by1)
			
			var aw=a.get_ancho()
			var bw=b.get_ancho() 
			res=false
				
			//recorre el cuadrado de intersección usando la mascara la mascara tiene la coordenada X reducida en 8.
			for var j=y0 to (y1-1)
				for var i=x0 to (x1-1)
					if ( a.mascara[(i-ax)  +(j-ay)* aw  ] != ' ' )  and  ( b.mascara[ (i-bx)+(j-by)*bw ] != ' ' )
						res=true
						break
	
	return res

def convertir_en_mascara (img:Gdk.Pixbuf):list of uchar
	str:list of uchar
	str = new list of uchar
	str.clear()
	////print"12345678901234567890"
	for var j=0 to (img.get_height()-1)
		for var i=0 to (img.get_width()-1)
			if is_alpha(i,j,img)
				str.add(' ')
			else
				str.add('x')
	return str



def get_rojo (color:string):double
	var c=color.split("#")
	return double.parse(c[0])
	

def get_verde (color:string):double
	var c=color.split("#")
	return double.parse(c[1])
	

def get_azul (color:string):double
	var c=color.split("#")
	return double.parse(c[2])
	
def convierte_a_string (r:double, g:double,b:double):string
	return r.to_string()+"#"+g.to_string()+"#"+b.to_string()


class Control: Object
	tipo:string
	pagina:int=0
	
	arrastrable:bool=true
	ampliable:bool=false
	editable:bool=false
	seleccionado:bool=false
	figurafondo:bool=false
	visible:bool=true
	
	valor_int:int=0
	valor_int2:int=0
	valor_int3:int=0
	valor_int4:int=0
	valor_int5:int=0
	valor_str:string
	valor_str2:string
	valor_str3:string
	valor_str4:string
	valor_str5:string
	valor_bool:bool
	valor_bool2:bool
	valor_bool3:bool
	valor_bool4:bool
	valor_bool5:bool
	
	x:int=0
	y:int=0
	ancho:int=0
	alto:int=0
	
	tomado_x:int
	tomado_y:int

	direccion:string
	archivo:string=""

	pixbufer: Gdk.Pixbuf=null
	pixbufer_original: Gdk.Pixbuf=null
	mascara: list of uchar
	
	fondo:bool=true
	borde:bool=false
	letra:bool=true
	colorfondo:string
	colorborde:string
	colorletra:string
	grosorborde:int=5
	tipoletra:string
	tamanotexto:double=20
	
	texto:  string
	cursor_buffer:int=0

	event izq_pulsado(c:Control)
	event der_pulsado(c:Control)
	event soltado(c:Control)
	
	
	init
		texto=" "
		tipo=""
		colorfondo="1#1#1"
		colorletra="0#0#0"
		colorborde="0#0#0"
		tipoletra="katamotzikasi"
		mascara= new list of uchar
		
	def virtual set_escala(ancho:int,alto:int)
		pass
	def virtual set_anchoborde(ancho:int)
		pass
	def virtual set_imagen (nombreImagen:string)
		pass
	def virtual set_tamanotexto(n:int)
		pass
	def virtual get_texto (): string
		return ""
	def virtual set_fondovisible (estado:bool)
		pass
	def virtual set_letravisible (estado:bool)
		pass
	def virtual set_texto (t:string)
		pass
	def virtual set_color_list(lista:list of int)
		pass
	def virtual set_borde(borde:bool)
		pass
	def virtual set_colorfondo(color:string)
		pass
	def virtual set_colorborde(color:string)
		pass
	def virtual set_colorletra(color:string)
		pass
	def virtual get_ancho():int
		return 0
	def virtual get_alto():int
		return 0
	def virtual get_centro_x():int
		return 0
	def virtual get_centro_y():int
		return 0
	def virtual set_centro_x(x:int)
		pass
	def virtual set_centro_y(x:int)
		pass
	def virtual set_ancho(w:int)
		pass
		
	def virtual set_alto(h:int)
		pass
	def virtual set_tamano(w:int, h:int)
		pass
	def virtual get_posx():int
		return 0
	def virtual get_posy():int
		return 0
	def virtual set_posx(n:int)
		pass
	def virtual set_posy(n:int)
		pass
	def virtual pinta (ctx:Cairo.Context)
		pass
	def virtual gestiona_tecla(valor:uint,tecla:string)
		pass
