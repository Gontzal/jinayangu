/* 
-------------------------------------------------------------------------
- Hasiera data: 2015ko otsailean                                         -
-
 <JinaYangu- Programa para trabajar la lectura y escritura>
    Copyright (C) <año>  <nombre del autor>

    Este programa es software libre: usted puede redistribuirlo y/o modificarlo 
    bajo los términos de la Licencia Pública General GNU publicada 
    por la Fundación para el Software Libre, ya sea la versión 3 
    de la Licencia, o (a su elección) cualquier versión posterior.

    Este programa se distribuye con la esperanza de que sea útil, pero 
    SIN GARANTÍA ALGUNA; ni siquiera la garantía implícita 
    MERCANTIL o de APTITUD PARA UN PROPÓSITO DETERMINADO. 
    Consulte los detalles de la Licencia Pública General GNU para obtener 
    una información más detallada. 
-------------------------------------------------------------------------
*/


class Ver_juego: Gtk.Window
	directorio_juego:string
	init
		pass
	def inicio()
		//entramos en directorio juegos
		GLib.Environment.set_current_dir (directorio_usuario+"/juegos")
		// Abrir un juego
		var FC=  new Gtk.FileChooserDialog (t.t("Abrir archivo:"), this, Gtk.FileChooserAction.OPEN,
		t.t("_Abrir"),Gtk.ResponseType.ACCEPT,
		t.t("_Cerrar"),Gtk.ResponseType.CANCEL);
		FC.select_multiple = false;
		FC.set_modal(true)
		FC.show()
		FC.response.connect(open)
		
	def open(d:Gtk.Dialog, response_id:int)
		var FC = d as Gtk.FileChooserDialog;
		case response_id
			when Gtk.ResponseType.CANCEL
				FC.hide()
				FC.close()
			when Gtk.ResponseType.ACCEPT
				FC.hide()
				directorio_juego=crear_directorio_tmp(directorio_usuario);
				descomprime_archivo(FC.get_filename(),directorio_juego)
				crear_pantalla()
				
	def crear_pantalla()
		// recoger los datos del archivo principal del juego.
		var lista_str_pantallas_elegidas = datos.abrir_archivo_pantallas (directorio_juego)
		var lista_str_actividades_elegidas = datos.abrir_archivo_actividades (directorio_juego)
		var lista_str_nombres_elegidos = datos.abrir_archivo_juego_nombres (directorio_juego)
		
		
		// Crear ventana
		var notebook= new Gtk.Notebook()
		var box_principal= new Gtk.Box(Gtk.Orientation.VERTICAL,5)
		box_principal.pack_start(notebook,true,true,0)
		this.add(box_principal)
		
		// primera pagina del notebook Actividades
		titulo1:Gtk.Label = new Gtk.Label ("Actividades del juego");
		contenido1 :Gtk.Box= new Gtk.Box (Gtk.Orientation.HORIZONTAL,5);
		
		var tree_actividades  = new Gtk.ListBox()
		
		var s0 = new Gtk.ScrolledWindow (null,null)
		s0.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)
		s0.set_min_content_height(250)
		s0.add(tree_actividades)
		
		contenido1.pack_start(s0,true,true,0)
		for var i=0 to ultimo_de_lista(lista_str_actividades_elegidas)
			var box=new Gtk.Box(Gtk.Orientation.HORIZONTAL,5)
			var label1= new Gtk.Label(lista_str_actividades_elegidas[i])
			box.pack_start(label1,true,true,0)
			tree_actividades.add(box)
		notebook.append_page (contenido1, titulo1);


		// Segunda parte del notebook: 
		titulo2:Gtk.Label = new Gtk.Label ("Pantallas");
		contenido2 :Gtk.Box= new Gtk.Box(Gtk.Orientation.HORIZONTAL, 5)
		
		var tree_pantallas2  = new Gtk.ListBox()
		var s2 = new Gtk.ScrolledWindow (null,null)
		s2.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)
		s2.set_min_content_height(250)
		s2.add(tree_pantallas2)
		
		contenido2.pack_start(s2,true,true,0)
		for var i=0 to ultimo_de_lista(lista_str_pantallas_elegidas)
			var box=new Gtk.Box(Gtk.Orientation.HORIZONTAL,5)
			var label= new Gtk.Label(lista_str_pantallas_elegidas[i])
			box.pack_start(label,true,true,0)
			tree_pantallas2.add(box)
		
		
		notebook.append_page (contenido2, titulo2);
		
		
		// Tercera parte del notebook:
		
		titulo3: Gtk.Label = new Gtk.Label ("Selección de nombres");
		contenido3 :Gtk.Box= new Gtk.Box (Gtk.Orientation.HORIZONTAL,5);
		
		var tree_actividades3  = new Gtk.ListBox()
		var s3 = new Gtk.ScrolledWindow (null,null)
		s3.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)
		s3.set_min_content_height(250)
		s3.add(tree_actividades3)
		
		contenido3.pack_start(s3,true,true,0)
		for var i=0 to ultimo_de_lista(lista_str_nombres_elegidos)
			var box=new Gtk.Box(Gtk.Orientation.HORIZONTAL,5)
			var label= new Gtk.Label(lista_str_nombres_elegidos[i])
			box.pack_start(label,true,true,0)
			tree_actividades3.add(box)
		notebook.append_page (contenido3, titulo3);

		
		
		//general
		this.show_all()
	
	
		
