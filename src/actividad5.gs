// Escuchar un nombre y escribirlo
class Actividad5:GLib.Object
	amigo:int
	solucion: list of int
	iman: Rectangulo
	escuchar:Imagen
	imagen: Imagen
	entrada: Entrada
	init
		pass
		
	def inicio(a:int)
		sonidos.play("sartu")
		amigo=a
		ventana_actividades.cargar_escenario()
		var nombre= selecciona_item_str_azar(lista_str_nombres)
		
		imagen=(new Imagen(ventana_actividades,100,30,juego.directorio_tmp_juego+"/nombres/"+nombre+"/imagen"))
		imagen.set_tamano(100,100)
		imagen.arrastrable=false
		imagen.valor_str=nombre
		imagen.soltado.connect(on_imagen)
		
		entrada= new Entrada(ventana_actividades,100,300,500,100,"")
		entrada.set_tamanotexto(50)
		
	
		escuchar= new Imagen(ventana_actividades,10,10,directorio_datos+"/imagenes/erabilgarri/play.png")
		escuchar.set_tamano(50,50)
		escuchar.arrastrable=false
		escuchar.valor_str=nombre
		escuchar.izq_pulsado.connect(on_escuchar)
	
		
		solucion= new list of int
		solucion.clear()
		
		var corregir=new Imagen(ventana_actividades,720,100,directorio_datos+"/imagenes/erabilgarri/zuzendu.png")
		corregir.set_tamano(50,50)
		corregir.arrastrable=false
		corregir.izq_pulsado.connect(on_corregir)
	
	
	def on_escuchar(c:Control)
		aplay(juego.directorio_tmp_juego+"/nombres/"+c.valor_str+"/0")
	
	def on_imagen(c:Control)
		aplay(juego.directorio_tmp_juego+"/nombres/"+c.valor_str+"/0")
	
		
		
	
			
	def on_corregir()
		var error=false
		if imagen.valor_str!=entrada.texto
			error=true
		
		if not error
			salir_bien()
		else
			salir_mal()
		
		
	def salir_bien()
		sonidos.play("ondo")
		juego.escenario[amigo].visible=false
		juego.amigos_conseguidos++
		if juego.amigos_conseguidos>=cuenta(juego.pantalla,"2")
			juego.escenario[juego.num_salida].visible=true
			
		juego.comienza_reloj()
		ventana_actividades.hide()
		ventana_general.show()
		
	def salir_mal()
		sonidos.play("gaizki")
		juego.comienza_reloj()
		ventana_actividades.hide()
		ventana_general.show()
		//si lo hace mal castigo volviendo al lugar de inicio y el amigo no se da por recogido
		juego.protagonista_a_inicio()
		juego.escenario[amigo].visible=true
	
