/* 
-------------------------------------------------------------------------
- Hasiera data: 2015ko otsailean                                         -
-
 <JinaYangu- Programa para trabajar la lectura y escritura>
    Copyright (C) <año>  <nombre del autor>

    Este programa es software libre: usted puede redistribuirlo y/o modificarlo 
    bajo los términos de la Licencia Pública General GNU publicada 
    por la Fundación para el Software Libre, ya sea la versión 3 
    de la Licencia, o (a su elección) cualquier versión posterior.

    Este programa se distribuye con la esperanza de que sea útil, pero 
    SIN GARANTÍA ALGUNA; ni siquiera la garantía implícita 
    MERCANTIL o de APTITUD PARA UN PROPÓSITO DETERMINADO. 
    Consulte los detalles de la Licencia Pública General GNU para obtener 
    una información más detallada. 
-------------------------------------------------------------------------
*/


class Crear_pantalla:Gtk.Window
	grid:Gtk.Grid
	str_pantalla:string
	hay_amigo:bool
	hay_protagonista:bool
	hay_salida:bool
	boton_fondo:Gtk.Button
	boton_protagonista: Gtk.Button
	boton_enemigo: Gtk.Button
	boton_enemigoV: Gtk.Button
	boton_enemigoH: Gtk.Button
	boton_salida: Gtk.Button
	boton_amigo: Gtk.Button
	pixbufer_blanco:Gdk.Pixbuf
	pixbufer_amigo:Gdk.Pixbuf
	pixbufer_enemigo:Gdk.Pixbuf
	pixbufer_enemigoH:Gdk.Pixbuf
	pixbufer_enemigoV:Gdk.Pixbuf
	pixbufer_protagonista:Gdk.Pixbuf
	pixbufer_salida:Gdk.Pixbuf
	pixbufer_fondo:Gdk.Pixbuf
	directorio_pantalla:string
	box:Gtk.Box
	init
		directorio_pantalla=""
		
		
	def inicio()
		directorio_pantalla=""
		box= new Gtk.Box(Gtk.Orientation.VERTICAL,10)
		var botones= new Gtk.Box(Gtk.Orientation.HORIZONTAL,5)
		var botones2= new Gtk.Box(Gtk.Orientation.HORIZONTAL,5)
		grid= new Gtk.Grid()
		box.add(grid)
		this.add(box)
		
		// crear pantallas
		var i=19
		var j=9
		for var xj=0 to 9
			for var xi=0 to 19
				i=19-xi
				j=9-xj
				try
					pixbufer_blanco=new Gdk.Pixbuf.from_file(directorio_datos+"/imagenes/erabilgarri/blanco.png")
					var imagen = new Gtk.Image.from_pixbuf (pixbufer_blanco.scale_simple(30,30, Gdk.InterpType.BILINEAR));		
					var boton_pantalla = new Gtk.Button()
					boton_pantalla.set_image(imagen)
					boton_pantalla.set_name("-")
					boton_pantalla.clicked.connect(on_boton_pantalla)
					grid.attach(boton_pantalla,i,j,1,1)
				except
					pass
				
		
		// crear botones
		box.add(botones)
		boton_fondo= new Gtk.Button()
		boton_protagonista= new Gtk.Button()
		boton_enemigo= new Gtk.Button()
		boton_enemigoV= new Gtk.Button()
		boton_enemigoH= new Gtk.Button()
		boton_salida= new Gtk.Button()
		boton_amigo= new Gtk.Button()
		
		botones.add(new Gtk.Label("Fondo"))
		botones.add(boton_fondo)
		botones.add(new Gtk.Label("Enemigo"))
		botones.add(boton_enemigo)
		botones.add(new Gtk.Label("Protagonista"))
		botones.add(boton_protagonista)
		botones.add(new Gtk.Label("Guardian Vertical"))
		botones.add(boton_enemigoV)
		botones.add(new Gtk.Label("Guardian Horizontal"))
		botones.add(boton_enemigoH)
		botones.add(new Gtk.Label("Salida"))
		botones.add(boton_salida)
		botones.add(new Gtk.Label("Amigo"))
		botones.add(boton_amigo)
		
		var boton_guardar= new Gtk.Button.with_label("Guardar")
		botones2.add(boton_guardar)
		var boton_abrir= new Gtk.Button.with_label("Abrir")
		botones2.add(boton_abrir)
		boton_guardar.clicked.connect(on_guardar)
		boton_abrir.clicked.connect(on_abrir_pantalla)
		box.add(botones2)
		this.show_all()
		
		//cargar el protagonista por defecto
		try
			pixbufer_protagonista=new Gdk.Pixbuf.from_file(directorio_datos+"/imagenes/erabilgarri/protagonista.png")
			var imagen = new Gtk.Image.from_pixbuf (pixbufer_protagonista.scale_simple(30,30, Gdk.InterpType.BILINEAR)	);		
			boton_protagonista.set_image(imagen)
			boton_protagonista.set_name("protagonista")
			boton_protagonista.clicked.connect(on_abrir_imagenes)
		except
			pass
		
		//cargar el enemigo por defecto
		try
			pixbufer_enemigo=new Gdk.Pixbuf.from_file(directorio_datos+"/imagenes/erabilgarri/enemigo.png")
			pixbufer_enemigo=pixbufer_enemigo.scale_simple(30,30, Gdk.InterpType.BILINEAR)	
			var imagen = new Gtk.Image.from_pixbuf (pixbufer_enemigo);		
			boton_enemigo.set_image(imagen)
			boton_enemigo.set_name("enemigo")
			boton_enemigo.clicked.connect(on_abrir_imagenes)
		except
			pass
		
		//cargar el guardianHorizontal por defecto
		try
			pixbufer_enemigoV=new Gdk.Pixbuf.from_file(directorio_datos+"/imagenes/erabilgarri/enemigoV.png")
			var imagen = new Gtk.Image.from_pixbuf (pixbufer_enemigoV.scale_simple(30,30, Gdk.InterpType.BILINEAR)	);		
			boton_enemigoV.set_image(imagen)
			boton_enemigoV.set_name("enemigoV")
			boton_enemigoV.clicked.connect(on_abrir_imagenes)
		except
			pass
		
		//cargar el guardianHorizontal por defecto
		try
			pixbufer_enemigoH=new Gdk.Pixbuf.from_file(directorio_datos+"/imagenes/erabilgarri/enemigoH.png")
			var imagen = new Gtk.Image.from_pixbuf (pixbufer_enemigoH.scale_simple(30,30, Gdk.InterpType.BILINEAR));		
			boton_enemigoH.set_image(imagen)
			boton_enemigoH.set_name("enemigoH")
			boton_enemigoH.clicked.connect(on_abrir_imagenes)
		except
			pass
		
		//cargar el amigo por defecto
		try
			pixbufer_amigo=new Gdk.Pixbuf.from_file(directorio_datos+"/imagenes/erabilgarri/amigo.png")
			var imagen = new Gtk.Image.from_pixbuf (pixbufer_amigo.scale_simple(30,30, Gdk.InterpType.BILINEAR)	);		
			boton_amigo.set_image(imagen)
			boton_amigo.set_name("amigo")
			boton_amigo.clicked.connect(on_abrir_imagenes)
		except
			pass
		
		
		//cargar el salida por defecto
		try
			pixbufer_salida=new Gdk.Pixbuf.from_file(directorio_datos+"/imagenes/erabilgarri/salida.png")
			
			var imagen = new Gtk.Image.from_pixbuf (pixbufer_salida.scale_simple(30,30, Gdk.InterpType.BILINEAR)	);		
			boton_salida.set_image(imagen)
			boton_salida.set_name("salida")
			boton_salida.clicked.connect(on_abrir_imagenes)
		except
			pass
		
		//cargar el fondo por defecto
		try
			pixbufer_fondo=new Gdk.Pixbuf.from_file(directorio_datos+"/imagenes/erabilgarri/fondo.jpg")
			var imagen = new Gtk.Image.from_pixbuf (pixbufer_fondo.scale_simple(30,30, Gdk.InterpType.BILINEAR)	);
			boton_fondo.set_image(imagen)
			boton_fondo.set_name("fondo")
			boton_fondo.clicked.connect(on_abrir_imagenes)
		except
			pass
		
	def on_boton_pantalla(btn:Gtk.Button)
		var x=btn.get_name()
		var img=""
		var secuencia= "-12345*-"
		for var i=0 to ultima(secuencia)
			var letra= toma_letra(secuencia,i)
			if x==letra
				x=toma_letra(secuencia,i+1)
				break
		case x
			when "-"
				img="blanco"
				var imagen = new Gtk.Image.from_pixbuf (pixbufer_blanco.scale_simple(30,30, Gdk.InterpType.BILINEAR));
				btn.set_image(imagen)
				btn.set_name(x)
			when "1"
				img="enemigo"
				var imagen = new Gtk.Image.from_pixbuf (pixbufer_enemigo.scale_simple(30,30, Gdk.InterpType.BILINEAR));
				btn.set_image(imagen)
				btn.set_name(x)
			when "2"
				img="amigo"
				var imagen = new Gtk.Image.from_pixbuf (pixbufer_amigo.scale_simple(30,30, Gdk.InterpType.BILINEAR));
				btn.set_image(imagen)
				btn.set_name(x)
			
			when "3"
				img="salida"
				var imagen = new Gtk.Image.from_pixbuf (pixbufer_salida.scale_simple(30,30, Gdk.InterpType.BILINEAR));
				btn.set_image(imagen)
				btn.set_name(x)
			
			when "4"
				img="enemigoH"
				var imagen = new Gtk.Image.from_pixbuf (pixbufer_enemigoH.scale_simple(30,30, Gdk.InterpType.BILINEAR));
				btn.set_image(imagen)
				btn.set_name(x)
			
			when "5"
				img="enemigoV"
				var imagen = new Gtk.Image.from_pixbuf (pixbufer_enemigoV.scale_simple(30,30, Gdk.InterpType.BILINEAR));
				btn.set_image(imagen)
				btn.set_name(x)
			
			when "*"
				img="protagonista"
				var imagen = new Gtk.Image.from_pixbuf (pixbufer_protagonista.scale_simple(30,30, Gdk.InterpType.BILINEAR));
				btn.set_image(imagen)
				btn.set_name(x)
			
		

			
					
	def on_abrir_pantalla()
		GLib.Environment.set_current_dir (directorio_usuario+"/pantallas")
		var FC=  new Gtk.FileChooserDialog (t.t("Abrir pantalla:"), this, Gtk.FileChooserAction.OPEN,
		t.t("_Abrir"),Gtk.ResponseType.ACCEPT,
		t.t("_Cerrar"),Gtk.ResponseType.CANCEL);
		
		FC.select_multiple = false;
		FC.set_modal(true)
		case FC.run ()
			when Gtk.ResponseType.CANCEL
				FC.hide()
				FC.close()
			when Gtk.ResponseType.ACCEPT
				FC.hide()
				var nombre_archivo=FC.get_filename()
				var directorio_tmp_pantalla = crear_directorio_tmp(directorio_usuario)
				descomprime_archivo(nombre_archivo,directorio_tmp_pantalla)
				str_pantalla=datos.abrir_pantalla(directorio_tmp_pantalla)
				try
					pixbufer_protagonista=new Gdk.Pixbuf.from_file(directorio_tmp_pantalla+"/protagonista")
					pixbufer_amigo=new Gdk.Pixbuf.from_file(directorio_tmp_pantalla+"/amigo")
					pixbufer_fondo=new Gdk.Pixbuf.from_file(directorio_tmp_pantalla+"/fondo")
					pixbufer_enemigo=new Gdk.Pixbuf.from_file(directorio_tmp_pantalla+"/enemigo")
					pixbufer_enemigoV=new Gdk.Pixbuf.from_file(directorio_tmp_pantalla+"/enemigoV")
					pixbufer_enemigoH=new Gdk.Pixbuf.from_file(directorio_tmp_pantalla+"/enemigoH")
					pixbufer_salida=new Gdk.Pixbuf.from_file(directorio_tmp_pantalla+"/salida")
					boton_amigo.set_image(new Gtk.Image.from_pixbuf (pixbufer_amigo.scale_simple(30,30, Gdk.InterpType.BILINEAR)))
					boton_protagonista.set_image(new Gtk.Image.from_pixbuf (pixbufer_protagonista.scale_simple(30,30, Gdk.InterpType.BILINEAR)))
					boton_fondo.set_image(new Gtk.Image.from_pixbuf (pixbufer_fondo.scale_simple(30,30, Gdk.InterpType.BILINEAR)))
					boton_enemigo.set_image(new Gtk.Image.from_pixbuf (pixbufer_enemigo.scale_simple(30,30, Gdk.InterpType.BILINEAR)))
					boton_enemigoH.set_image(new Gtk.Image.from_pixbuf (pixbufer_enemigoH.scale_simple(30,30, Gdk.InterpType.BILINEAR)))
					boton_enemigoV.set_image(new Gtk.Image.from_pixbuf (pixbufer_enemigoV.scale_simple(30,30, Gdk.InterpType.BILINEAR)))
					boton_salida.set_image(new Gtk.Image.from_pixbuf (pixbufer_salida.scale_simple(30,30, Gdk.InterpType.BILINEAR)))
					for var i=0 to 9
						grid.remove_row(0)
					// crear pantallas
					var i=19
					var j=9
					var cont=199
					for var xj=0 to 9
						for var xi=0 to 19
							i=19-xi
							j=9-xj
							case toma_letra(str_pantalla,cont)
								when "-"
									var imagen = new Gtk.Image.from_pixbuf (pixbufer_blanco.scale_simple(30,30, Gdk.InterpType.BILINEAR));		
									var boton_pantalla = new Gtk.Button()
									boton_pantalla.set_image(imagen)
									boton_pantalla.set_name("-")
									boton_pantalla.clicked.connect(on_boton_pantalla)
									grid.attach(boton_pantalla,i,j,1,1)
								when "*"
									var imagen = new Gtk.Image.from_pixbuf (pixbufer_protagonista.scale_simple(30,30, Gdk.InterpType.BILINEAR));		
									var boton_pantalla = new Gtk.Button()
									boton_pantalla.set_image(imagen)
									boton_pantalla.set_name("*")
									boton_pantalla.clicked.connect(on_boton_pantalla)
									grid.attach(boton_pantalla,i,j,1,1)
								when "1"
									var imagen = new Gtk.Image.from_pixbuf (pixbufer_enemigo.scale_simple(30,30, Gdk.InterpType.BILINEAR));		
									var boton_pantalla = new Gtk.Button()
									boton_pantalla.set_image(imagen)
									boton_pantalla.set_name("1")
									boton_pantalla.clicked.connect(on_boton_pantalla)
									grid.attach(boton_pantalla,i,j,1,1)
								when "2"
									var imagen = new Gtk.Image.from_pixbuf (pixbufer_amigo.scale_simple(30,30, Gdk.InterpType.BILINEAR));		
									var boton_pantalla = new Gtk.Button()
									boton_pantalla.set_image(imagen)
									boton_pantalla.set_name("2")
									boton_pantalla.clicked.connect(on_boton_pantalla)
									grid.attach(boton_pantalla,i,j,1,1)
								when "3"
									var imagen = new Gtk.Image.from_pixbuf (pixbufer_salida.scale_simple(30,30, Gdk.InterpType.BILINEAR));		
									var boton_pantalla = new Gtk.Button()
									boton_pantalla.set_image(imagen)
									boton_pantalla.set_name("3")
									boton_pantalla.clicked.connect(on_boton_pantalla)
									grid.attach(boton_pantalla,i,j,1,1)
								when "4"
									var imagen = new Gtk.Image.from_pixbuf (pixbufer_enemigoH.scale_simple(30,30, Gdk.InterpType.BILINEAR));		
									var boton_pantalla = new Gtk.Button()
									boton_pantalla.set_image(imagen)
									boton_pantalla.set_name("4")
									boton_pantalla.clicked.connect(on_boton_pantalla)
									grid.attach(boton_pantalla,i,j,1,1)
								when "5"
									var imagen = new Gtk.Image.from_pixbuf (pixbufer_enemigoV.scale_simple(30,30, Gdk.InterpType.BILINEAR));		
									var boton_pantalla = new Gtk.Button()
									boton_pantalla.set_image(imagen)
									boton_pantalla.set_name("5")
									boton_pantalla.clicked.connect(on_boton_pantalla)
									grid.attach(boton_pantalla,i,j,1,1)
							cont--
				except
					pass
				grid.show_all()
				this.show_all()
				
		
	def on_guardar()
		
		str_pantalla=""
		hay_amigo=false;hay_protagonista=false;	hay_salida=false
		grid.foreach(on_foreach_grid)
		if hay_amigo and hay_protagonista and hay_salida
			GLib.Environment.set_current_dir (directorio_usuario+"/pantallas")
			var FC=  new Gtk.FileChooserDialog (("Guardar Pantalla:"), this, Gtk.FileChooserAction.SAVE,
			t.t("_Guardar"),Gtk.ResponseType.ACCEPT,
			t.t("_Cerrar"),Gtk.ResponseType.CANCEL);
			
			FC.select_multiple = false;
			FC.set_modal(true)
			case FC.run ()
				when Gtk.ResponseType.CANCEL
					FC.hide()
					FC.close()
				when Gtk.ResponseType.ACCEPT
					FC.hide()
					directorio_pantalla = crear_directorio_tmp(directorio_usuario);
					crea_archivo_escribiendo(directorio_pantalla+"/archivo",str_pantalla)
					try
						pixbufer_protagonista.scale_simple(40,40, Gdk.InterpType.BILINEAR).save(directorio_pantalla+"/protagonista","png")
						pixbufer_amigo.scale_simple(40,40, Gdk.InterpType.BILINEAR).save(directorio_pantalla+"/amigo","png")
						pixbufer_fondo.save(directorio_pantalla+"/fondo","png")
						pixbufer_enemigo.scale_simple(40,40, Gdk.InterpType.BILINEAR).save(directorio_pantalla+"/enemigo","png")
						pixbufer_enemigoV.scale_simple(40,40, Gdk.InterpType.BILINEAR).save(directorio_pantalla+"/enemigoV","png")
						pixbufer_enemigoH.scale_simple(40,40, Gdk.InterpType.BILINEAR).save(directorio_pantalla+"/enemigoH","png")
						pixbufer_salida.scale_simple(40,40, Gdk.InterpType.BILINEAR).save(directorio_pantalla+"/salida","png")
					except
						pass
					comprime_directorio (directorio_pantalla,FC.get_filename()+".kjp")
					
					
		else
			print "En cada pantalla tiene que haber minimamente un amigo, un protagonista y una salida."
		
	def on_foreach_grid (w:Gtk.Widget)
		str_pantalla+=w.get_name()
		if w.get_name()=="*" do hay_protagonista=true
		if w.get_name()=="2" do hay_amigo=true
		if w.get_name()=="3" do hay_salida=true
		
	def on_abrir_imagenes(btn:Gtk.Button)
		GLib.Environment.set_current_dir (directorio_usuario+"/pantallas")
			
		var FC=  new Gtk.FileChooserDialog (t.t("abrir :")+" "+btn.get_name(), this, Gtk.FileChooserAction.OPEN,
		t.t("_Abrir"),Gtk.ResponseType.ACCEPT,
		t.t("_Cerrar"),Gtk.ResponseType.CANCEL);
		FC.select_multiple = false;
		FC.set_modal(true)
		case FC.run ()
			when Gtk.ResponseType.CANCEL
				FC.hide()
				FC.close()
			when Gtk.ResponseType.ACCEPT
				FC.hide()
				try
					case btn.get_name()
						when "amigo"
							pixbufer_amigo=	new Gdk.Pixbuf.from_file(FC.get_filename())
							var imagen = new Gtk.Image.from_pixbuf (pixbufer_amigo.scale_simple(30,30, Gdk.InterpType.BILINEAR));
							btn.set_image(imagen)
						when "enemigo"
							pixbufer_enemigo=new Gdk.Pixbuf.from_file(FC.get_filename())
							var imagen = new Gtk.Image.from_pixbuf (pixbufer_enemigo.scale_simple(30,30, Gdk.InterpType.BILINEAR)	);
							btn.set_image(imagen)
						when "enemigoV"
							pixbufer_enemigoV=new Gdk.Pixbuf.from_file(FC.get_filename())
							var imagen = new Gtk.Image.from_pixbuf (pixbufer_enemigoV.scale_simple(30,30, Gdk.InterpType.BILINEAR));
							btn.set_image(imagen)
						when "enemigoH"
							pixbufer_enemigoH=new Gdk.Pixbuf.from_file(FC.get_filename())
							var imagen = new Gtk.Image.from_pixbuf (pixbufer_enemigoH.scale_simple(30,30, Gdk.InterpType.BILINEAR));
							btn.set_image(imagen)
						when "salida"
							pixbufer_salida=new Gdk.Pixbuf.from_file(FC.get_filename())
							var imagen = new Gtk.Image.from_pixbuf (pixbufer_salida.scale_simple(30,30, Gdk.InterpType.BILINEAR));
							btn.set_image(imagen)
						when "protagonista"
							pixbufer_protagonista=new Gdk.Pixbuf.from_file(FC.get_filename())
							var imagen = new Gtk.Image.from_pixbuf (pixbufer_protagonista.scale_simple(30,30, Gdk.InterpType.BILINEAR));		
							btn.set_image(imagen)
						
						when "fondo"
							pixbufer_fondo=new Gdk.Pixbuf.from_file(FC.get_filename())
							var imagen = new Gtk.Image.from_pixbuf (pixbufer_fondo.scale_simple(30,30, Gdk.InterpType.BILINEAR));		
							btn.set_image(imagen)
						
							
				except
					print "error al intentar tomar esa imagen"
		
