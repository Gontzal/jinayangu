class Juego:GLib.Object
	protagonista: Imagen
	escenario: list of Imagen
	mireloj:uint
	px:int=0;
	py:int=0
	directorio_tmp_juego:string
	directorio_tmp_pantalla:string
	numpantalla:int
	pantallas_juego:list of string
	pantalla:string
	amigos_conseguidos:int=0
	num_salida:int=-1
	init 
		escenario= new list of Imagen
		pantallas_juego= new list of string
		
	def inicio()
		numpantalla=0
		GLib.Environment.set_current_dir (directorio_usuario+"/juegos")
		//Abrir archivo
		var FC=  new Gtk.FileChooserDialog (("Abrir archivo:"), ventana_general , Gtk.FileChooserAction.OPEN,
			t.t("_Abrir"),Gtk.ResponseType.ACCEPT,
			t.t("_Cerrar"),Gtk.ResponseType.CANCEL);
			
		FC.select_multiple = false;
		FC.set_modal(true)
		FC.show()
		FC.response.connect(open)
		
	def open(d:Gtk.Dialog, response_id:int)
		var FC = d as Gtk.FileChooserDialog;
		case response_id
			when Gtk.ResponseType.CANCEL
				FC.hide()
				FC.close()
			when Gtk.ResponseType.ACCEPT
				FC.hide()
				var nombre_archivo=FC.get_filename ();
				directorio_tmp_juego = crear_directorio_tmp(directorio_usuario)
				datos.cargar_juego (nombre_archivo,directorio_tmp_juego)
				numpantalla=0
				directorio_tmp_pantalla = crear_directorio_tmp(directorio_usuario)
				descomprime_archivo(directorio_tmp_juego+"/pantallas/"+pantallas_juego[numpantalla]+".kjp",directorio_tmp_pantalla)
				pantalla=datos.abrir_pantalla(directorio_tmp_pantalla)
				datos.abrir_juego_nombres(directorio_tmp_juego)
				ventana_general.cargar_escenario()
				crear_escenario()
				
	def crear_escenario()
		amigos_conseguidos=0
		num_salida=-1
		escenario.clear()
		//fondo
		var fondo= new Imagen(ventana_general,0,0,directorio_tmp_pantalla+"/fondo")
		fondo.set_tamano(1000,500)
		fondo.figurafondo=true
		fondo.arrastrable=false
		
		var ex=0;var ey=0
		for var i=0 to 199
			var car=toma_letra(pantalla,i)
			case car
				when "1"
					escenario.add(new Imagen(ventana_general,ex,ey,directorio_tmp_pantalla+"/enemigo"))
					escenario.last().arrastrable=false
					escenario.last().figurafondo=true
					escenario.last().valor_str="enemigo"
				when "2"
					escenario.add(new Imagen(ventana_general,ex,ey,directorio_tmp_pantalla+"/amigo"))
					escenario.last().arrastrable=false
					escenario.last().figurafondo=true
					escenario.last().valor_str="amigo"
					
				when "3"
					escenario.add(new Imagen(ventana_general,ex,ey,directorio_tmp_pantalla+"/salida"))
					escenario.last().arrastrable=false
					escenario.last().figurafondo=true
					escenario.last().visible=false
					escenario.last().valor_str="salida"
					num_salida=i
					print num_salida.to_string()
				
				when "4"
					//malo con movimiento horizontal
					escenario.add(new Imagen(ventana_general,ex,ey,directorio_tmp_pantalla+"/enemigoH"))
					escenario.last().arrastrable=false
					escenario.last().figurafondo=true
					escenario.last().valor_str="enemigoH"
					escenario.last().valor_int=ex
					escenario.last().valor_int2=ey
					escenario.last().valor_int3=Random.int_range(5,25)
					
				when "5"
					//malo con movimiento vertical
					escenario.add(new Imagen(ventana_general,ex,ey,directorio_tmp_pantalla+"/enemigoV"))
					escenario.last().arrastrable=false
					escenario.last().figurafondo=true
					escenario.last().valor_str="enemigoV"
					escenario.last().valor_int=ex
					escenario.last().valor_int2=ey
					escenario.last().valor_int3=Random.int_range(5,25)
				when "*" // este es el protagonista pero lo pondremos más tarde
					escenario.add(new Imagen(ventana_general,ex,ey,directorio_datos+"/imagenes/erabilgarri/blanco.png")) // no cololcamos la imagen de prota.
					escenario.last().arrastrable=false
					escenario.last().figurafondo=true
					escenario.last().valor_str="blanco"
					px=ex
					py=ey
					
				default
					escenario.add(new Imagen(ventana_general,ex,ey,directorio_datos+"/imagenes/erabilgarri/blanco.png"))
					escenario.last().arrastrable=false
					escenario.last().figurafondo=true
					escenario.last().valor_str="blanco"
					
			if (i+1)%20==0
				ey+=50
				ex=0
			else
				ex+=50
		
		// colocamos la nave espacial al final para que quede sobre todos.
		protagonista= new Imagen(ventana_general,px,py,directorio_tmp_pantalla+"/protagonista")
		protagonista.arrastrable=false
		
		// boton_salir
		var boton_salir= new Imagen(ventana_general,10,520,directorio_datos+"/imagenes/erabilgarri/atzera.png")
		boton_salir.set_tamano(50,50)
		boton_salir.izq_pulsado.connect(on_salir)
		boton_salir.arrastrable=false
		//controlar el teclado
		ventana_general.key_press_event.connect(on_teclado)
		//controlar relo
		comienza_reloj()
		
	
	def comienza_reloj()
		mireloj = Timeout.add(1250, mover)
		pass
		
	def mover ():bool
		
		for var i=0 to ultimo_de_lista(escenario)
			if escenario[i].valor_str=="enemigoH" // La nave horizontal se movera en un ratio de 100 pixels
				if (escenario[i].get_posx()) < (escenario[i].valor_int-100) or (escenario[i].get_posx()) > (escenario[i].valor_int+100)
					escenario[i].valor_int3=-escenario[i].valor_int3
				escenario[i].set_posx( escenario[i].get_posx()+escenario[i].valor_int3)	
			if escenario[i].valor_str=="enemigoV"// La nave vertical se movera en un ratio de 100 pixels
				if (escenario[i].get_posy()) < (escenario[i].valor_int2-100) or (escenario[i].get_posy()) > (escenario[i].valor_int2+100)
					escenario[i].valor_int3=-escenario[i].valor_int3
				escenario[i].set_posy( escenario[i].get_posy()+escenario[i].valor_int3)	
			
			case escenario[i].valor_str
				when "enemigo","enemigoV","enemigoH"
					if colision_mascara (protagonista, escenario[i])
						sonidos.play("blub")
						protagonista_a_inicio()
				
		ventana_general.lienzo.queue_draw()
		return true
		
	def protagonista_a_inicio()
		protagonista.set_posx(px)
		protagonista.set_posy(py)
		
	def on_teclado(evt:Gdk.EventKey):bool
		var tecla= evt.keyval
		if tecla==65363 and protagonista.get_posx()<950 // mueve protagonista a la derecha
			protagonista.set_posx(protagonista.get_posx()+10)
			
		if tecla==65361 and protagonista.get_posx()>20 // mueve protagonista a la izquierda
			protagonista.set_posx(protagonista.get_posx()-10)
			
		if tecla==65362 and protagonista.get_posy()>0 // mueve protagonista a la arriba
			protagonista.set_posy(protagonista.get_posy()-10)
			
		if tecla==65364 and protagonista.get_posy()<450 // mueve protagonista a la abajo
			protagonista.set_posy(protagonista.get_posy()+10)
			
		//print "tecla:"+tecla.to_string()
		for var i=0 to (ultimo_de_lista(escenario))
			case escenario[i].valor_str
				when "enemigo","enemigoV","enemigoH"
					if colision_mascara (protagonista, escenario[i])
						sonidos.play("blub")
						protagonista.set_posx(px)
						protagonista.set_posy(py)
				when "amigo"
					if colision_mascara (protagonista, escenario[i])
						Source.remove(mireloj)
						//activamos un ejercicio
						ventana_actividades=new Cairo_interface();ventana_actividades.inicio(900,500); 
						comienza_actividad_al_azar(i);
						ventana_actividades.set_deletable(false);ventana_actividades.set_resizable(false)
						ventana_general.hide()
						
				when "salida"
					if colision_mascara (protagonista, escenario[i])
						
						numpantalla++
						print "pasamos a :"+numpantalla.to_string()
						if ultimo_de_lista(pantallas_juego)>=numpantalla // si todavía hay pantallas cargar con otra
							pantalla=datos.abrir_pantalla(directorio_tmp_juego+"/pantallas/"+pantallas_juego[numpantalla]+".kjp")
							Source.remove(mireloj)
							ventana_general.key_press_event.disconnect(on_teclado)
							ventana_general.cargar_escenario()
							crear_escenario()
						else // salir y escribir game over
							ventana_general.key_press_event.disconnect(on_teclado)
							Source.remove(mireloj)
							ventana_general.cargar_escenario()
							
							var texto= new Texto(ventana_general,350,200,"Lo has conseguido.Game over")
							texto.tipoletra="Andika"
							texto.arrastrable=false
							texto.izq_pulsado.connect(on_salir)
		
							
		ventana_general.lienzo.queue_draw()
		return true
	
		
	def on_delete_event_true():bool
		return true
	
	def comienza_actividad_al_azar(a:int)
		var activazar=Random.int_range(1,6+1)
		case activazar
			when 1
				actividad1.inicio(a)
			when 2
				actividad2.inicio(a)
			when 3
				actividad3.inicio(a)
			when 4
				actividad4.inicio(a)
			when 5
				actividad5.inicio(a)
			when 6
				actividad6.inicio(a)
			when 7
				actividad7.inicio(a)
	def on_salir()
		Source.remove(mireloj)
		pantalla_inicio.inicio()
		
		
