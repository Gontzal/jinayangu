uses Gtk

class Crear_juego:Gtk.Window
	box_principal: Gtk.Box
	notebook: Gtk.Notebook
	directorio_juego:string
	check_nombres:list of Gtk.CheckButton
	check_actividades:list of Gtk.CheckButton
	boton_pantallas2:list of Gtk.Button
	boton_pantallasx2:list of Gtk.Button
	num_posicion:int=0
	s2:Gtk.ScrolledWindow 
	tree_pantallas2:Gtk.ListBox
	sx2:Gtk.ScrolledWindow 
	tree_pantallasx2:Gtk.ListBox
	lista_str_pantallas_elegidas:list of string
	init
		check_nombres= new list of Gtk.CheckButton
		check_actividades= new list of Gtk.CheckButton
		boton_pantallas2= new list of Gtk.Button
		boton_pantallasx2= new list of Gtk.Button
		lista_str_pantallas_elegidas= new list of string
		
	def inicio()
		num_posicion=0
		//crear variables
		directorio_juego=crear_directorio_tmp(directorio_usuario);
		lista_str_pantallas=datos.abrir_directorio_pantallas(directorio_usuario)
		
		// Crear ventana
		notebook= new Gtk.Notebook()
		box_principal= new Gtk.Box(Gtk.Orientation.VERTICAL,5)
		box_principal.pack_start(notebook,true,true,0)
		this.add(box_principal)
		
		// primera pagina del notebook Actividades
		titulo1:Gtk.Label = new Gtk.Label ("Actividades del juego");
		contenido1 :Gtk.Box= new Gtk.Box (Gtk.Orientation.HORIZONTAL,5);
		
		var tree_actividades  = new Gtk.ListBox()
		
		var s0 = new Gtk.ScrolledWindow (null,null)
		s0.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)
		s0.set_min_content_height(250)
		s0.add(tree_actividades)
		
		contenido1.pack_start(s0,true,true,0)
		for var i=0 to ultimo_de_lista(lista_str_actividades)
			var box=new Box(Gtk.Orientation.HORIZONTAL,5)
			var label1= new Label(lista_str_actividades[i])
			check_actividades.add(new Gtk.CheckButton())
			box.pack_start(label1,true,true,0)
			box.pack_start(check_actividades.last(),false,true,0)
			tree_actividades.add(box)
		notebook.append_page (contenido1, titulo1);


		// Segunda parte del notebook: 
		titulo2:Gtk.Label = new Gtk.Label ("Pantallas");
		contenido2 :Box= new Gtk.Box(Gtk.Orientation.HORIZONTAL, 5)
		
		tree_pantallas2  = new Gtk.ListBox()
		s2 = new Gtk.ScrolledWindow (null,null)
		s2.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)
		s2.set_min_content_height(250)
		s2.add(tree_pantallas2)
		
		contenido2.pack_start(s2,true,true,0)
		for var i=0 to ultimo_de_lista(lista_str_pantallas)
			var box=new Box(Gtk.Orientation.HORIZONTAL,5)
			var label= new Label(lista_str_pantallas[i])
			boton_pantallas2.add(new Gtk.Button.with_label("->"))
			boton_pantallas2.last().set_name(lista_str_pantallas[i])
			boton_pantallas2.last().clicked.connect(on_mete_pantallas)
			box.pack_start(label,true,true,0)
			box.pack_start(boton_pantallas2.last(),false,true,0)
			tree_pantallas2.add(box)
		
		tree_pantallasx2  = new Gtk.ListBox()
		sx2 = new Gtk.ScrolledWindow (null,null)
		sx2.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)
		sx2.set_min_content_height(250)
		sx2.add(tree_pantallasx2)
		contenido2.pack_start(sx2,true,true,10)
		
		notebook.append_page (contenido2, titulo2);
		
		
		// Tercera parte del notebook:
		
		titulo3:Gtk.Label = new Gtk.Label ("Selección de nombres");
		contenido3 :Gtk.Box= new Gtk.Box (Gtk.Orientation.HORIZONTAL,5);
		
		var tree_actividades3  = new Gtk.ListBox()
		var s3 = new Gtk.ScrolledWindow (null,null)
		s3.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)
		s3.set_min_content_height(250)
		s3.add(tree_actividades3)
		
		contenido3.pack_start(s3,true,true,0)
		for var i=0 to ultimo_de_lista(lista_str_nombres)
		
			var box=new Box(Gtk.Orientation.HORIZONTAL,5)
			var label= new Label(lista_str_nombres[i])
			check_nombres.add(new Gtk.CheckButton())
			box.pack_start(label,true,true,0)
			box.pack_start(check_nombres.last(),false,true,0)
			tree_actividades3.add(box)
		notebook.append_page (contenido3, titulo3);

		//botones
		var boton_guardar_juego= new Button.with_label("Guardar juego")
		boton_guardar_juego.clicked.connect(on_boton_guardar_juego)
		var boton_ayuda= new Button.with_label("Ayuda")
		
		var box_botones= new Box (Gtk.Orientation.HORIZONTAL, 5)
		box_botones.add(boton_guardar_juego)
		box_botones.add(boton_ayuda)
		box_botones.set_homogeneous(true)
		box_principal.pack_start(box_botones,true,true,0)
		
		//general
		this.show_all()
		this.destroy.connect(on_salir)
	
	// elimina pantallas de tree_pantallasx2.
	def on_quita_pantallas(w:Gtk.Widget)
		tree_pantallasx2.remove(w.get_parent().get_parent())
		tree_pantallasx2.show_all()
	
	// añade pantallas a la parte derecha llamada tree_pantallax2
	def on_mete_pantallas(w:Gtk.Widget)
		var row=new Gtk.ListBoxRow()
		row.set_name(w.get_name())
		var box=new Box(Gtk.Orientation.HORIZONTAL,5) 
		var label= new Label(w.get_name()) 
		boton_pantallasx2.add(new Gtk.Button.with_label("X")) 
		boton_pantallasx2.last().clicked.connect(on_quita_pantallas) 
		box.pack_start(label,true,true,0)
		box.pack_start(boton_pantallasx2.last(),false,true,0) 
		row.add(box)
		tree_pantallasx2.add(row)
		tree_pantallasx2.show_all()
		
				
		
	def on_boton_guardar_juego()
		//borra todo lo anterior en los directorios del juego.
		borrar_directorio_interior_juego()
		
		//crear la fila1 del archivo
		var fila1=""
		for var i=0 to ultimo_de_lista(lista_str_nombres)
			if check_nombres[i].get_active()==true
				fila1+=lista_str_nombres[i]+"*-*"
		fila1=toma_cadena(fila1,0,longitud(fila1)-3)
		
		//crear la fila2 del archivo
		var fila2=""
		for var i=0 to ultimo_de_lista(lista_str_actividades)
			if check_actividades[i].get_active()==true
				fila2+=i.to_string()+"*-*"
		fila2=toma_cadena(fila2,0,longitud(fila2)-3)
		
		//Con este bucle se recorre la LisBox de pantallas elegidas e introduce los nombres de la pantallas en la lista ....
		lista_str_pantallas_elegidas.clear()
		tree_pantallasx2.foreach(bucle_pantallas)
		
		//crear la fila3 del archivo
		var fila3=""
		for var i=0 to ultimo_de_lista(lista_str_pantallas_elegidas)
			fila3+=lista_str_pantallas_elegidas[i]+"*-*"
		fila3=toma_cadena(fila3,0,longitud(fila3)-3)
		
		if fila1=="" or fila2=="" or fila3==""
			// Si ha dejado algo sin rellenar no se puede crear el archivo.
			var m= new Gtk.MessageDialog (this, Gtk.DialogFlags.MODAL, Gtk.MessageType.WARNING, Gtk.ButtonsType.OK, t.t("Faltan datos no será grabado"));
			m.run()
			m.close()
		else
			//creando el archivo
			crea_archivo_escribiendo(directorio_juego+"/archivo",fila1+"\n"+fila2+"\n"+fila3+"\n")
			crea_directorio(directorio_juego+"/nombres")
			crea_directorio(directorio_juego+"/pantallas")
			
			//recorre la lista nombres y copia los archivos necesarios kjn
			for var i=0 to ultimo_de_lista(lista_str_nombres)
				if check_nombres[i].get_active()
					copia_archivo(directorio_usuario+"/nombres/"+lista_str_nombres[i]+".kjn",directorio_juego+"/nombres/"+lista_str_nombres[i]+".kjn")
			//recorre la lista pantallas elegidas y copia los archivos necesarios kjp
			for var i=0 to ultimo_de_lista(lista_str_pantallas_elegidas)
				copia_archivo(directorio_usuario+"/pantallas/"+lista_str_pantallas_elegidas[i]+".kjp",directorio_juego+"/pantallas/"+lista_str_pantallas_elegidas[i]+".kjp")
			// crea archivo tar del juego kjj.
			GLib.Environment.set_current_dir (directorio_usuario+"/juegos")
			
			var FC=  new FileChooserDialog (t.t("Crear archivo:"), this, Gtk.FileChooserAction.SAVE,
			t.t("_Crear"),Gtk.ResponseType.ACCEPT,
			t.t("_Cerrar"),Gtk.ResponseType.CANCEL);
			FC.select_multiple = false;
			FC.set_modal(true)
			case FC.run ()
				when Gtk.ResponseType.CANCEL
					FC.hide()
					FC.close()
				when Gtk.ResponseType.ACCEPT
					FC.hide()
					comprime_directorio(directorio_juego,FC.get_filename()+".kjj")
	
	def bucle_pantallas(w:Widget)
		//Con este bucle se recorre la LisBox de pantallas elegidas e introduce los nombres de la pantallas en la lista ....
		lista_str_pantallas_elegidas.add(w.get_name())
	def on_salir()
		borrar_directorio_interior_juego()
		borrar_directorio_vacio_juego()
		
	def borrar_directorio_interior_juego()
		borra_directorio_interior(directorio_juego+"/nombres")
		borra_directorio_interior(directorio_juego+"/pantallas")
		
		borra_directorio_vacio(directorio_juego+"/nombres")
		borra_directorio_vacio(directorio_juego+"/pantallas")
		
		borra_directorio_interior(directorio_juego)

	def borrar_directorio_vacio_juego()
		borra_directorio_vacio(directorio_juego)
		
		

		
	
