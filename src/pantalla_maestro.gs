/* 
-------------------------------------------------------------------------
- Hasiera data: 2015ko otsailean                                         -
-
 <JinaYangu- Programa para trabajar la lectura y escritura>
    Copyright (C) <año>  <nombre del autor>

    Este programa es software libre: usted puede redistribuirlo y/o modificarlo 
    bajo los términos de la Licencia Pública General GNU publicada 
    por la Fundación para el Software Libre, ya sea la versión 3 
    de la Licencia, o (a su elección) cualquier versión posterior.

    Este programa se distribuye con la esperanza de que sea útil, pero 
    SIN GARANTÍA ALGUNA; ni siquiera la garantía implícita 
    MERCANTIL o de APTITUD PARA UN PROPÓSITO DETERMINADO. 
    Consulte los detalles de la Licencia Pública General GNU para obtener 
    una información más detallada. 
-------------------------------------------------------------------------
*/


class Pantalla_maestro:GLib.Object
	texto:TextoCuadro
	init 
		pass
	def inicio()
		cargar_escenario()
		
		var rec1= new Rectangulo (ventana_general,50,100,280,300)
		rec1.set_colorfondo(verde)
		var rec2= new Rectangulo (ventana_general,350,100,280,300)
		rec2.set_colorfondo(amarillo)
		var rec3= new Rectangulo (ventana_general,650,100,280,300)
		rec3.set_colorfondo(rojo)
		
		var tit1= new Texto (ventana_general,120,50,"JUEGOS")
		tit1.tipoletra="Andika"
		tit1.arrastrable=false
		tit1.izq_pulsado.connect(on_tit1)
		var tit2= new Texto (ventana_general,420,50,"NOMBRES")
		tit2.tipoletra="Andika"
		tit2.arrastrable=false
		tit2.izq_pulsado.connect(on_tit2)
		var tit3= new Texto (ventana_general,720,50,"PANTALLAS")
		tit3.tipoletra="Andika"
		tit3.arrastrable=false
		tit3.izq_pulsado.connect(on_tit3)
		
		texto= new TextoCuadro (ventana_general,50,420,900,100,"Los juegos se configuran en tres partes actividades, pantallas y nombres. Para que el/la alumno/a pueda pasar de pantalla tiene que recoger una serie de objetos/personas y esquivar enemigos. Por cada objeto/persona que recoja tiene que superar una actividad al azar entre las seleccionadas por el/la maestro/a")
		texto.set_tamanotexto(10)
		texto.tipoletra="Andika"
		texto.arrastrable=false
		texto.colorfondo=verde
		
		
		
		var opcion_maestra1= new Texto(ventana_general,80,120,"Crear un juego")
		opcion_maestra1.arrastrable=false
		opcion_maestra1.tipoletra="Andika"
		opcion_maestra1.izq_pulsado.connect(on_opcion_maestra1)
		
		var opcion_maestra2= new Texto(ventana_general,80,180,"Ver un juego")
		opcion_maestra2.arrastrable=false
		opcion_maestra2.tipoletra="Andika"
		opcion_maestra2.izq_pulsado.connect(on_opcion_maestra2)
	
		var opcion_maestra3= new Texto(ventana_general,80,240,"Borrar juego")
		opcion_maestra3.arrastrable=false
		opcion_maestra3.tipoletra="Andika"
		opcion_maestra3.izq_pulsado.connect(on_opcion_maestra3)
		
		var opcion_maestra5= new Texto(ventana_general,400,120,"Crear nombres")
		opcion_maestra5.arrastrable=false
		opcion_maestra5.tipoletra="Andika"
		opcion_maestra5.izq_pulsado.connect(on_opcion_maestra5)
		
		var opcion_maestra6= new Texto(ventana_general,400,180,"Borrar nombres")
		opcion_maestra6.arrastrable=false
		opcion_maestra6.tipoletra="Andika"
		opcion_maestra6.izq_pulsado.connect(on_opcion_maestra6)
		
		var opcion_maestra7= new Texto(ventana_general,400,240,"Modificar nombres")
		opcion_maestra7.arrastrable=false
		opcion_maestra7.tipoletra="Andika"
		opcion_maestra7.izq_pulsado.connect(on_opcion_maestra7)
		
		var opcion_maestra8= new Texto(ventana_general,700,120,"Crear pantallas")
		opcion_maestra8.arrastrable=false
		opcion_maestra8.tipoletra="Andika"
		opcion_maestra8.izq_pulsado.connect(on_opcion_maestra8)
		
		var opcion_maestra9= new Texto(ventana_general,700,180,"Borrar pantallas")
		opcion_maestra9.arrastrable=false
		opcion_maestra9.tipoletra="Andika"
		opcion_maestra9.izq_pulsado.connect(on_opcion_maestra9)
		
		var opcion_maestra10= new Texto(ventana_general,700,240,"Modificar pantallas")
		opcion_maestra10.arrastrable=false
		opcion_maestra10.tipoletra="Andika"
		opcion_maestra10.izq_pulsado.connect(on_opcion_maestra10)
		
		var opcion_salir= new Imagen (ventana_general,20,550,directorio_datos+"/imagenes/erabilgarri/atzera.png")
		opcion_salir.set_tamano(40,40)
		opcion_salir.arrastrable=false
		opcion_salir.tipoletra="Andika"
		opcion_salir.izq_pulsado.connect(on_opcion_salir)
		
		
	def on_opcion_maestra1 (c:Control)
		crear_juego.inicio()

	def on_opcion_maestra2(c:Control)
		ver_juego.inicio()
		
	def on_opcion_maestra3(c:Control)
		GLib.Environment.set_current_dir (directorio_usuario+"/juegos")
		var FC=  new Gtk.FileChooserDialog (t.t("Borrar juego:"), ventana_general, Gtk.FileChooserAction.OPEN,
		t.t("_Borrar"),Gtk.ResponseType.ACCEPT,
		t.t("_Cerrar"),Gtk.ResponseType.CANCEL);
		FC.select_multiple = false;
		FC.set_modal(true)
		FC.show()
		FC.response.connect(open3)
		
	def open3(d:Gtk.Dialog, response_id:int)
		var FC = d as Gtk.FileChooserDialog;
		case response_id
			when Gtk.ResponseType.CANCEL
				FC.hide()
				FC.close()
			when Gtk.ResponseType.ACCEPT
				FC.hide()
				var nombre_archivo=FC.get_filename()
				borra_archivo(nombre_archivo)
		
	
	def on_opcion_maestra5(c:Control)
		crear_nombre.inicio()
	
	def on_opcion_maestra6(c:Control)
		GLib.Environment.set_current_dir (directorio_usuario+"/nombres")
		var FC=  new Gtk.FileChooserDialog (t.t("Borrar nombre:"),ventana_general, Gtk.FileChooserAction.OPEN,
		t.t("_Borrar"),Gtk.ResponseType.ACCEPT,
		t.t("_Cerrar"),Gtk.ResponseType.CANCEL);
		FC.select_multiple = false;
		FC.set_modal(true)
		FC.show()
		FC.response.connect(open6)
		
	def open6(d:Gtk.Dialog, response_id:int)
		var FC = d as Gtk.FileChooserDialog;
		case response_id
			when Gtk.ResponseType.CANCEL
				FC.hide()
				FC.close()
			when Gtk.ResponseType.ACCEPT
				FC.hide()
				var nombre_archivo=FC.get_filename()
				borra_archivo(nombre_archivo)
		
	
	def on_opcion_maestra7(c:Control)
		crear_nombre.inicio()
		crear_nombre.on_abrir()
	
	def on_opcion_maestra8(c:Control)
		crear_pantalla.inicio()
		
	def on_opcion_maestra9(c:Control)
		GLib.Environment.set_current_dir (directorio_usuario+"/pantallas")
		var FC=  new Gtk.FileChooserDialog (t.t("Borrar pantalla:"), ventana_general, Gtk.FileChooserAction.OPEN,
		t.t("_Borrar"),Gtk.ResponseType.ACCEPT,
		t.t("_Cerrar"),Gtk.ResponseType.CANCEL);
		FC.select_multiple = false;
		FC.set_modal(true)
		FC.show()
		FC.response.connect(open9)
		
	def open9(d:Gtk.Dialog, response_id:int)
		var FC = d as Gtk.FileChooserDialog;
		case response_id
			when Gtk.ResponseType.CANCEL
				FC.hide()
				FC.close()
			when Gtk.ResponseType.ACCEPT
				FC.hide()
				var nombre_archivo=FC.get_filename()
				borra_archivo(nombre_archivo)
		
		
	def on_opcion_maestra10(c:Control)
		crear_pantalla.inicio()
		crear_pantalla.on_abrir_pantalla()
	
	def on_opcion_salir()
		pantalla_inicio.inicio()
		
	def cargar_escenario()
		ventana_general.controles.clear()
	
	def on_tit1()
		texto.set_texto(t.t("Los juegos se configuran en tres partes actividades, pantallas y nombres. Para que el/la alumno/a pueda pasar de pantalla tiene que recoger una serie de objetos/personas y esquivar enemigos. Por cada objeto/persona que recoja tiene que superar una actividad al azar entre las seleccionadas por el/la maestro/a"))
		texto.colorfondo=verde
	def on_tit2()
		texto.set_texto(t.t("Los nombres son aquellas palabras que pueden ser objetos/personas/lugares/verbos y serán usadas en las actividades. El programa aporta algunos pocos nombres por defecto. Sin embargo se considera recomendable que el/la maestro/a cree sus propios nombres. Es especialmente recomendable para niños que no han iniciado la lectoescritura la familiarización con los nombres de los/as compañeros/as de clase. Para la creación de nombres se precisará de un buen micrófono, puesto que la calidad del sonido es importante. Se grabará el nombre, cada una de las sílabas de ese nombre y una pequeña descripción que no será en ningún caso despectiva."));
		texto.colorfondo=amarillo
		
	def on_tit3()
		texto.set_texto(t.t("Las pantallas son los escenarios donde transcurre el juego. Puedes configurarlos al gusto de tus alumnos y alumnas eligiendo las imagenes de fondo que desees y los personajes que prefieras. Procura que las imagenes de tus personajes tengan fondo transparente. Puedes crear pantallas sencillas o complicadas. Como mínimo cada pantalla tendrá que tener un lugar de comienzo para el protagonista, un amigo/objeto que recoger y una salida para pasar a la siguiente pantalla. Existen tres tipos de enemigos inmóviles, móviles verticales y móviles horizontales."));
		texto.colorfondo=rojo
	
	
	

