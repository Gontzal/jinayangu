/* 
-------------------------------------------------------------------------
- Hasiera data: 2015ko otsailean                                         -
-
 <Jinayangu- Programa para trabajar la lectura y escritura>
    Copyright (C) <año>  <nombre del autor>

    Este programa es software libre: usted puede redistribuirlo y/o modificarlo 
    bajo los términos de la Licencia Pública General GNU publicada 
    por la Fundación para el Software Libre, ya sea la versión 3 
    de la Licencia, o (a su elección) cualquier versión posterior.

    Este programa se distribuye con la esperanza de que sea útil, pero 
    SIN GARANTÍA ALGUNA; ni siquiera la garantía implícita 
    MERCANTIL o de APTITUD PARA UN PROPÓSITO DETERMINADO. 
    Consulte los detalles de la Licencia Pública General GNU para obtener 
    una información más detallada. 
-------------------------------------------------------------------------
*/




uses Gee;
uses Pango;
uses SDL;

activada:bool
sonidos:Sonidos
directorio_datos:string
directorio_sonidos:string
directorio_usuario:string
nombre_programa:string
datos:Datos
ventana_general:Cairo_interface
ventana_actividades:Cairo_interface
juego:Juego
actividad1:Actividad1;actividad2:Actividad2;actividad3:Actividad3;actividad4:Actividad4;actividad5:Actividad5;actividad6:Actividad6;actividad7:Actividad7;
pantalla_inicio:Pantalla_inicio
pantalla_maestro:Pantalla_maestro
crear_juego: Crear_juego
ver_juego:Ver_juego
crear_nombre: Crear_nombre

crear_pantalla:Crear_pantalla
mayusculas:bool=false // por defecto se mostrará todo en minusculas
t:Traduccion

init 
	
	
	// inicializamos el controlador de sonido SDL o Gst
	Gtk.init(ref args)
	SDLMixer.open(44100,SDL.AudioFormat.S16LSB,2,4096);
	
	//inicializa variables de directorios
	nombre_programa="jinayangu-1.0"
	directorio_datos=GLib.Environment.get_current_dir()+"/.."
	directorio_sonidos="/sonidos/zaratak/"
	directorio_usuario= GLib.Environment.get_home_dir()+"/"+nombre_programa
	
	// detectar el idioma del sistema y traducir todas las frases entrecomilladas con t.t("") delante.
	t= new Traduccion("es") // programa escrito en es: Español
	t.traduce() // busca lenguas locales y traduce.

	// abriendo archivos que se necesitan para el programa.
	datos= new Datos()
	datos.abriendo_archivos_necesarios()
	
	//Sonidos
	sonidos=new Sonidos()
	
	// crea variables con nombre de colores
	inicializa_colores()
	
	//inicializa pantallas
	ventana_general= new Cairo_interface();ventana_general.inicio(1000,600); ventana_general.set_resizable(false); ventana_general.set_deletable(false) // Gestion de interface cairo
	pantalla_inicio= new Pantalla_inicio(); // En esta pantalla se muestra un alumno y un profesor para ser elegido.
	pantalla_maestro= new Pantalla_maestro(); // En esta pantalla el maestro/a crea, borra y modifica juegos.
	crear_juego= new Crear_juego(); crear_juego.set_transient_for(ventana_general); crear_juego.set_modal(true);crear_juego.hide() // Crear juegos
	ver_juego= new Ver_juego(); ver_juego.set_transient_for(ventana_general); ver_juego.set_modal(true);ver_juego.hide() // Ver juegos
	crear_nombre= new Crear_nombre(); crear_nombre.set_transient_for(ventana_general); crear_nombre.set_modal(true);crear_nombre.hide() //Crear nombres
	crear_pantalla= new Crear_pantalla(); crear_pantalla.set_transient_for(ventana_general); crear_pantalla.set_modal(true);crear_pantalla.hide() //Crear pantallas
	juego= new Juego()
	actividad1= new Actividad1()
	actividad2= new Actividad2()
	actividad3= new Actividad3()
	actividad4= new Actividad4()
	actividad5= new Actividad5()
	actividad6= new Actividad6()
	actividad7= new Actividad7()
	pantalla_inicio.inicio() // Comienza el programa enseñando dos opciones: Maestro o Alumno
	Gtk.main()

	
	
