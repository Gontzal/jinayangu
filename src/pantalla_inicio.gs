class Pantalla_inicio:GLib.Object
	imagen_maestra: Imagen
	imagen_alumno: Imagen
	imagen_salir: Imagen
	init 
		pass
	def inicio()
		ventana_general.cargar_escenario()
		
		imagen_maestra= new Imagen(ventana_general,210,200,directorio_datos+"/imagenes/erabilgarri/maestra.png")
		imagen_maestra.arrastrable=false
		imagen_maestra.izq_pulsado.connect(on_imagen_maestra)
		
		imagen_alumno= new Imagen (ventana_general,650,200,directorio_datos+"/imagenes/erabilgarri/ikasle.png")
		imagen_alumno.arrastrable=false
		imagen_alumno.izq_pulsado.connect(on_imagen_alumno)
		
		imagen_salir= new Imagen (ventana_general,20,550,directorio_datos+"/imagenes/erabilgarri/atzera.png")
		imagen_salir.arrastrable=false
		imagen_salir.set_tamano(40,40)
		imagen_salir.izq_pulsado.connect(on_imagen_salir)
		
		
	def on_imagen_maestra(c:Control)
		//sonidos.play("blub")
		pantalla_maestro.inicio()
		
	def on_imagen_alumno(c:Control)
		//sonidos.play("blub")
		juego.inicio()
		
	def on_imagen_salir(c:Control)
		//borra todos los archivos temporales creados
		try
			print "rm -rf "+directorio_usuario+"/.*"
			Process.spawn_command_line_async ("find /home/gontzal/jinayangu-1.0 -name .* -exec rm -rf {} ;")
			print "fin"
		except
			print "no pudo ser"
		// saliendo
		Gtk.main_quit()
		
	
	
	
	
	

