// Escuchar un nombre y escribirlo
class Actividad4:GLib.Object
	amigo:int
	solucion: list of int
	iman: Rectangulo
	escuchar:Imagen
	imagen: Imagen
	letras: list of Texto
	pista: Texto
	init
		pass
		
	def inicio(a:int)
		sonidos.play("sartu")
		amigo=a
		ventana_actividades.cargar_escenario()
		var nombre= selecciona_item_str_azar(lista_str_nombres)
		var nombre_desordenado=desordena_string((nombre))
		
		imagen=(new Imagen(ventana_actividades,100,30,juego.directorio_tmp_juego+"/nombres/"+nombre+"/imagen"))
		imagen.set_tamano(100,100)
		imagen.arrastrable=false
		imagen.valor_str=nombre
		imagen.soltado.connect(on_imagen)
		
		pista= new Texto(ventana_actividades,300,30,nombre)
		pista.arrastrable=false
		pista.set_tamanotexto(50)
		
		letras= new list of Texto
		letras.clear()
		
		for var i=0 to ultima(nombre)
			letras.add(new Texto(ventana_actividades,100+i*50,200,toma_letra(nombre_desordenado,i)))
			letras.last().arrastrable=true
			letras.last().valor_str=toma_letra(nombre_desordenado,i)
			letras.last().valor_int=i
			letras.last().soltado.connect (on_soltar_letra)
			letras.last().izq_pulsado.connect(on_tomar_letra)
			letras.last().set_tamanotexto(30)
		
		
		escuchar= new Imagen(ventana_actividades,10,10,directorio_datos+"/imagenes/erabilgarri/play.png")
		escuchar.set_tamano(50,50)
		escuchar.arrastrable=false
		escuchar.valor_str=nombre
		escuchar.izq_pulsado.connect(on_escuchar)
	
		iman= new Rectangulo (ventana_actividades, 100,300,520,120)
		iman.arrastrable=false
		iman.valor_str=""
		
		solucion= new list of int
		solucion.clear()
		
		var corregir=new Imagen(ventana_actividades,720,100,directorio_datos+"/imagenes/erabilgarri/zuzendu.png")
		corregir.set_tamano(50,50)
		corregir.arrastrable=false
		corregir.izq_pulsado.connect(on_corregir)
	
	
	def on_escuchar(c:Control)
		aplay(juego.directorio_tmp_juego+"/nombres/"+c.valor_str+"/0")
	
	def on_imagen(c:Control)
		aplay(juego.directorio_tmp_juego+"/nombres/"+c.valor_str+"/0")
	
	def on_soltar_letra(c:Control)
		if colision_cuadrada(iman,c)
			c.valor_bool=true
			solucion.add(c.valor_int)
			c.set_posy((iman.get_posy()+iman.get_alto()/2)-(c.get_alto()/2))
		else
			c.valor_bool=false
			if solucion.contains(c.valor_int)
				solucion.remove_at(solucion.index_of(c.valor_int))
				
		organiza_iman()
		
	def on_tomar_letra(c:Control)
		c.valor_bool=false
		if solucion.contains(c.valor_int)
			solucion.remove_at(solucion.index_of(c.valor_int))
		
		organiza_iman()
		
	def organiza_iman()
		var pos=15+iman.get_posx()
		for var i=0 to ultimo_de_lista(solucion)
			if letras[solucion[i]].valor_bool
				letras[solucion[i]].set_posx(pos)
				pos+=letras[solucion[i]].get_ancho()+2


				
			
	def on_corregir()
		var error=false
		if (ultima(imagen.valor_str)!=ultimo_de_lista(solucion))
			error=true
		else
			for var i=0 to ultimo_de_lista(solucion)
				imagen.valor_str+=letras[solucion[i]].valor_str
				if letras[solucion[i]].valor_str != toma_letra(imagen.valor_str,i)
					error=true
		
		if not error
			salir_bien()
		else
			salir_mal()
		
		
	def salir_bien()
		sonidos.play("ondo")
		juego.escenario[amigo].visible=false
		juego.amigos_conseguidos++
		if juego.amigos_conseguidos>=cuenta(juego.pantalla,"2")
			juego.escenario[juego.num_salida].visible=true
			
		juego.comienza_reloj()
		ventana_actividades.hide()
		ventana_general.show()
		
	def salir_mal()
		sonidos.play("gaizki")
		juego.comienza_reloj()
		ventana_actividades.hide()
		ventana_general.show()
		//si lo hace mal castigo volviendo al lugar de inicio y el amigo no se da por recogido
		juego.protagonista_a_inicio()
		juego.escenario[amigo].visible=true
	
