/* 
-------------------------------------------------------------------------
- Hasiera data: 2015ko otsailean                                         -
-
 <JinaYangu- Programa para trabajar la lectura y escritura>
    Copyright (C) <año>  <nombre del autor>

    Este programa es software libre: usted puede redistribuirlo y/o modificarlo 
    bajo los términos de la Licencia Pública General GNU publicada 
    por la Fundación para el Software Libre, ya sea la versión 3 
    de la Licencia, o (a su elección) cualquier versión posterior.

    Este programa se distribuye con la esperanza de que sea útil, pero 
    SIN GARANTÍA ALGUNA; ni siquiera la garantía implícita 
    MERCANTIL o de APTITUD PARA UN PROPÓSITO DETERMINADO. 
    Consulte los detalles de la Licencia Pública General GNU para obtener 
    una información más detallada. 
-------------------------------------------------------------------------
*/


lista_str_actividades:list of string
lista_str_nombres:list of string
lista_str_pantallas:list of string
class Datos:Object
	init
		lista_str_actividades= new list of string
		lista_str_nombres= new list of string
		lista_str_pantallas= new list of string
		pantalla1:string
	def abriendo_archivos_necesarios()
		abrir_directorio_nombres()
		abrir_archivo_base_actividades()
		
	def abrir_archivo_base_actividades()
		lista_str_actividades.clear()
		var f = FileStream.open(directorio_datos+"/palabras/Actividades","r")
		if f==null
			print "archivo de Actividades no encontrado "
		else
			print "archivo de Actividades encontrado"
			var c=""
			c=f.read_line()
			var sil=c.split("-")
			for var i=0 to ultimo_de_array(sil)
				lista_str_actividades.add(sil[i])
		
	def abrir_directorio_nombres()
		lista_str_nombres.clear()
		try
			var d=Dir.open(directorio_usuario+"/nombres")
			var encontrado=false
			var name=""
			while ( (name = d.read_name()) != null)
				if toma_cadena(name,ultima(name)-3,longitud(name))==".kjn" 
					encontrado=true
					lista_str_nombres.add(toma_cadena(name,0,longitud(name)-4))
		except
			print "no se pudo abrir directorio de nombres"
			
	def abrir_directorio_pantallas(dir:string):list of string
		var l= new list of string
		try
			var d=Dir.open(dir+"/pantallas")
			var encontrado=false
			var name=""
			while ( (name = d.read_name()) != null)
				if toma_cadena(name,ultima(name)-3,longitud(name))==".kjp" 
					encontrado=true
					l.add(toma_cadena(name,0,longitud(name)-4))
					print name
		except
			print "no se pudo abrir directorio de nombres"
		return l

	def abrir_archivo_pantallas(dir:string):list of string
		var l= new list of string
		var f = FileStream.open(dir+"/archivo","r")
		if f==null
			print "archivo no encontrado "
		else
			print "archivo encontrado"
			var c=""
			c=f.read_line()
			c=f.read_line()
			c=f.read_line()
			var pan=c.split("*-*")
			for var i=0 to ultimo_de_array(pan)
				l.add(pan[i])
		
		return l

	def abrir_archivo_actividades(dir:string):list of string
		var l= new list of string
		var f = FileStream.open(dir+"/archivo","r")
		if f==null
			print "archivo no encontrado "
		else
			print "archivo encontrado"
			var c=""
			c=f.read_line()
			c=f.read_line()
			var pan=c.split("*-*")
			for var i=0 to ultimo_de_array(pan)
				var n=int.parse(pan[i])
				l.add(lista_str_actividades[n])
		
		return l


	def abrir_archivo_juego_nombres(dir:string):list of string
		var l= new list of string
		var f = FileStream.open(dir+"/archivo","r")
		if f==null
			print "archivo no encontrado "
		else
			print "archivo encontrado"
			var c=""
			c=f.read_line()
			var pan=c.split("*-*")
			for var i=0 to ultimo_de_array(pan)
				l.add(pan[i])
		return l

	def abrir_archivo_nombre(dir:string):list of string
		var l= new list of string
		var f = FileStream.open(dir+"/nombre","r")
		if f==null
			print "archivo no encontrado "
		else
			print "archivo encontrado"
			var c=""
			for var i=0 to 6
				c=f.read_line()
				l.add(c)
		return l

	
	def abrir_pantalla(direc:string):string
		var pantalla=""
		var f = FileStream.open(direc+"/archivo","r")
		if f==null
			print "archivo de Actividades no encontrado "
		else
			print "archivo de Actividades encontrado"
			var c=""
			for var i=0 to 9
				c+=f.read_line()
			pantalla=c
		return pantalla

	def abrir_juego_nombres(dir:string)
		for var i=0 to ultimo_de_lista(lista_str_nombres)
			crea_directorio(dir+"/nombres/"+lista_str_nombres[i])
			descomprime_archivo(dir+"/nombres/"+lista_str_nombres[i]+".kjn",dir+"/nombres/"+lista_str_nombres[i])

	def conocer_silabas(dir:string,nombre:string):list of string
		var lista= new list of string
		var f = FileStream.open(dir+"/"+nombre+"/nombre","r")
		if f!=null
			f.read_line() //esta linea es del nombre que será ignorado
			while not f.eof()
				var c=f.read_line()
				if not f.eof() and c!="" 
					lista.add(c)
		return lista
	def silaba_numero(dir:string,nombre:string,s:string):string
		var f = FileStream.open(dir+"/"+nombre+"/nombre","r")
		var n=1
		if f!=null
			f.read_line() //esta linea es del nombre que será ignorado
			while not f.eof()
				var c=f.read_line()
				
				if not f.eof() and c!="" 
					if c==s 
						break
					else
						n++
		return n.to_string()
		
	def conocer_nombre(dir:string,nombre:string):string
		var nom=""
		var f = FileStream.open(dir+"/"+nombre+"/nombre","r")
		if f!=null
			nom=f.read_line() //esta linea es del nombre
		return nom
		
	def cargar_juego(direc:string,direc_destino:string)
		descomprime_archivo (direc,direc_destino)
		juego.pantallas_juego=abrir_archivo_pantallas(direc_destino)

	
