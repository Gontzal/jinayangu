def crea_nombre():string
	var now = new GLib.DateTime.now_local ();
	var fecha_str = now.get_day_of_month ().to_string()+now.get_month ().to_string()+now.get_year ().to_string()+now.get_hour().to_string()+now.get_minute().to_string()+now.get_seconds().to_string()
	return fecha_str.replace(".","")
			
def lista_archivos (direc:string):list of string
	var r= new list of string
	var name=""
	try
		var d = Dir.open( direc+"/" )
		while ((name = d.read_name()) != null) 
			r.add(direc+"/"+name)
	except
		pass
	return r


	
def comprime_directorio(directorio:string,nombre_archivo:string)
	try
		// recogemos el directorio actual para poder volver a el en el futuro
		var cd= GLib.Environment.get_current_dir ()
		// nos desplazamos al directorio del usuario para recoger desde alli los archivos temporales y crear un comprimido tar
		GLib.Environment.set_current_dir (directorio)
		Process.spawn_command_line_sync ("tar -cf "+nombre_archivo+" .")
		GLib.Environment.set_current_dir (cd)
	except
		pass

def descomprime_archivo(nombre_archivo:string,directorio_destino:string)
	try
		// recogemos el directorio actual para poder volver a el en el futuro
		var cd= GLib.Environment.get_current_dir ()
		// nos desplazamos al directorio del usuario para recoger desde alli los archivos temporales y crear un comprimido tar
		GLib.Environment.set_current_dir (directorio_destino)
		Process.spawn_command_line_sync ("tar -xf "+nombre_archivo)
		GLib.Environment.set_current_dir (cd)
	except
		print "error al descomprimir"

def crea_archivo_escribiendo(directorio:string,texto:string)
	var f = FileStream.open(directorio,"w")
	f.puts(texto)
	
def aplay(direc:string)
	try
		Process.spawn_command_line_async ("aplay "+direc)
	except
		pass
def arecord(direc:string)
	try
		Process.spawn_command_line_async ("arecord "+direc)
	except
		pass		
def stop_aplay()
	try
		Process.spawn_command_line_async ("killall aplay")
	except
		pass
def stop_arecord()
	try
		Process.spawn_command_line_async ("killall arecord")
	except
		pass

def copia_archivo (a1:string,a2:string):bool
	var error=false
	
	if a1!=a2
		try 
			
			var arch1= File.new_for_path(a1)
			var arch2= File.new_for_path(a2)
			arch1.copy (arch2,0,null)
		except
			print "no se pudo copiar el archivo"+a1 +" a " + a2
			error=true
	return error
def lista_directorio (direc:string):list of string
	var r= new list of string
	var name=""
	try
		var d = Dir.open( direc+"/" )
		while ((name = d.read_name()) != null) 
			r.add(name)
	except
		pass
	
	return r
	
def borra_directorio_interior (direc:string):bool
	r:bool=true
	var name=""
	try
		var d = Dir.open( direc+"/" )
		while ((name = d.read_name()) != null) 
			FileUtils.remove(direc+"/"+name)
	except
		r=false
	return r
	
	
def borra_archivo (direc:string):bool
	FileUtils.remove(direc)
	return true
	
def borra_directorio_vacio (direc:string):bool
	FileUtils.remove(direc)
	return true
	

def existe_directorio (direc:string):bool
	r:bool=false
	var name=""
	var partes= direc.split("/")
	var nombre=partes[ultimo_de_array(partes)] 
	var dir=""
	for var i=0 to (ultimo_de_array(partes)-1)
		dir+=partes[i]+"/"
	try
		var d = Dir.open( dir )
		while ((name = d.read_name()) != null) 
			if nombre==name
				r=true
				break
	except
		r=false
	return r

def existe_archivo (direc:string):bool
	r:bool=false
	var name=""
	var partes= direc.split("/")
	var nombre=partes[ultimo_de_array(partes)] 
	var dir=""
	for var i=0 to (ultimo_de_array(partes)-1)
		dir+=partes[i]+"/"
	try
		var d = Dir.open( dir )
		while ((name = d.read_name()) != null) 
			if nombre==name
				r=true
				break
	except
		r=false
	return r
	
def crea_directorio (direc:string):bool
	var r=true
	try	
		var dir = File.new_for_commandline_arg (direc)
		dir.make_directory ()
	except
		r=false
	return r
def crear_directorio_tmp(dir:string):string
		var r=""
		r=dir+"/."+crea_nombre()
		crea_directorio(r)
		return r
