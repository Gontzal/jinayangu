// Escuchar un nombre y escribirlo
class Actividad6:GLib.Object
	amigo:int
	solucion: list of int
	iman: Rectangulo
	escuchar:Imagen
	imagen: Imagen
	letras: list of Texto
	opciones: list of string
	op:int
	letra:string
	init
		pass
		
	def inicio(a:int)
		sonidos.play("sartu")
		amigo=a
		ventana_actividades.cargar_escenario()
		var nombre= selecciona_item_str_azar(lista_str_nombres)
		var lugar=Random.int_range(0,ultima(nombre))
		letra= toma_letra(nombre,lugar)
		var nombre2 = sobrescribe_letra(nombre,"?",lugar)
		opciones= new list of string
		opciones.add(letra)
		op=0
		for var i=0 to 3
			opciones.add(toma_letra("abcdefghijklmnopqrstuvwxyz",Random.int_range(0,27)))
		
		imagen=(new Imagen(ventana_actividades,100,30,juego.directorio_tmp_juego+"/nombres/"+nombre+"/imagen"))
		imagen.set_tamano(100,100)
		imagen.arrastrable=false
		imagen.valor_str=nombre
		imagen.soltado.connect(on_imagen)
		
		letras= new list of Texto
		letras.clear()
		
		for var i=0 to ultima(nombre)
			
			letras.add(new Texto(ventana_actividades,100+i*70,200,toma_letra(nombre2,i)))
			letras.last().arrastrable=false
			letras.last().valor_str=toma_letra(nombre2,i)
			letras.last().valor_int=i
			letras.last().izq_pulsado.connect(on_tomar_letra)
			letras.last().set_tamanotexto(50)
			if toma_letra(nombre2,i)!="?"
				letras.last().colorfondo=blanco
			else
				letras.last().colorfondo=rojo
		
			
			
		
		
		escuchar= new Imagen(ventana_actividades,10,10,directorio_datos+"/imagenes/erabilgarri/play.png")
		escuchar.set_tamano(50,50)
		escuchar.arrastrable=false
		escuchar.valor_str=nombre
		escuchar.izq_pulsado.connect(on_escuchar)
	
		
		solucion= new list of int
		solucion.clear()
		
		var corregir=new Imagen(ventana_actividades,720,100,directorio_datos+"/imagenes/erabilgarri/zuzendu.png")
		corregir.set_tamano(50,50)
		corregir.arrastrable=false
		corregir.izq_pulsado.connect(on_corregir)
	
	
	def on_escuchar(c:Control)
		aplay(juego.directorio_tmp_juego+"/nombres/"+c.valor_str+"/0")
	
	def on_imagen(c:Control)
		aplay(juego.directorio_tmp_juego+"/nombres/"+c.valor_str+"/0")
	
		
	def on_tomar_letra(c:Control)
		op++
		if op>ultimo_de_lista(opciones) do op=0
		c.set_texto(opciones[op])
				
			
	def on_corregir()
		var error=false
		if opciones[op]!=letra
			error=true
		
		if not error
			salir_bien()
		else
			salir_mal()
		
		
	def salir_bien()
		sonidos.play("ondo")
		juego.escenario[amigo].visible=false
		juego.amigos_conseguidos++
		if juego.amigos_conseguidos>=cuenta(juego.pantalla,"2")
			juego.escenario[juego.num_salida].visible=true
			
		juego.comienza_reloj()
		ventana_actividades.hide()
		ventana_general.show()
		
	def salir_mal()
		sonidos.play("gaizki")
		juego.comienza_reloj()
		ventana_actividades.hide()
		ventana_general.show()
		//si lo hace mal castigo volviendo al lugar de inicio y el amigo no se da por recogido
		juego.protagonista_a_inicio()
		juego.escenario[amigo].visible=true
	
