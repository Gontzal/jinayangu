class Actividad1:GLib.Object
	amigo:int
	iman:Rectangulo
	escuchar:Imagen
	imagen: list of Imagen
	init
		pass
		
	def inicio(a:int)
		sonidos.play("sartu")
		amigo=a
		ventana_actividades.cargar_escenario()
		// colocamos la nave espacial al final para que quede sobre todos.
		var lista_nombres= selecciona_varios_str_azar(3,lista_str_nombres)
		var nombre_correcto= selecciona_item_str_azar(lista_nombres)
		imagen= new list of Imagen
		imagen.clear()
		for var i=0 to ultimo_de_lista(lista_nombres)
			imagen.add(new Imagen(ventana_actividades,100+i*120,100,juego.directorio_tmp_juego+"/nombres/"+lista_nombres[i]+"/imagen"))
			imagen.last().set_tamano(100,100)
			imagen.last().arrastrable=true
			imagen.last().valor_str=lista_nombres[i]
			imagen.last().valor_int=i
			imagen.last().soltado.connect(on_imagen)
		
		escuchar= new Imagen(ventana_actividades,10,10,directorio_datos+"/imagenes/erabilgarri/play.png")
		escuchar.set_tamano(50,50)
		escuchar.arrastrable=false
		escuchar.valor_str=nombre_correcto
		escuchar.izq_pulsado.connect(on_escuchar)
	
		iman= new Rectangulo (ventana_actividades, 250,250,120,120)
		iman.arrastrable=false
		iman.valor_str=""
		
		var corregir=new Imagen(ventana_actividades,720,100,directorio_datos+"/imagenes/erabilgarri/zuzendu.png")
		corregir.set_tamano(50,50)
		corregir.arrastrable=false
		corregir.izq_pulsado.connect(on_corregir)
	
	
	def on_escuchar(c:Control)
		aplay(juego.directorio_tmp_juego+"/nombres/"+c.valor_str+"/0")
	
	def on_imagen(c:Control)
		
		if colision_cuadrada(c, iman) 
			if (iman.valor_str=="")
				c.set_centro_x( iman.get_centro_x() )
				c.set_centro_y( iman.get_centro_y() )
				sonidos.play("blub")
				iman.valor_str=c.valor_str
			else
				var cont=0
				for var i=0 to ultimo_de_lista(imagen) 
					if colision_cuadrada(imagen[i],iman) do cont++
				if cont==1 // hay mas de dos figuras tocando el iman.
					c.set_centro_x( iman.get_centro_x() )
					c.set_centro_y( iman.get_centro_y() )
					sonidos.play("blub")
					iman.valor_str=c.valor_str
		
			
				
			
	def on_corregir()
		if iman.valor_str==escuchar.valor_str
			salir_bien()
		else
			salir_mal()
		
		
	def salir_bien()
		sonidos.play("ondo")
		juego.escenario[amigo].visible=false
		juego.amigos_conseguidos++
		if juego.amigos_conseguidos>=cuenta(juego.pantalla,"2")
			juego.escenario[juego.num_salida].visible=true
			
		juego.comienza_reloj()
		ventana_actividades.hide()
		ventana_general.show()
		
	def salir_mal()
		sonidos.play("gaizki")
		juego.comienza_reloj()
		ventana_actividades.hide()
		ventana_general.show()
		//si lo hace mal castigo volviendo al lugar de inicio y el amigo no se da por recogido
		juego.protagonista_a_inicio()
		juego.escenario[amigo].visible=true
	
