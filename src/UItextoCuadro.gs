uses Cairo
uses Gtk
uses Pango

class TextoCuadro: Control
	construct (escenario:Cairo_interface,posicion_x:int,posicion_y:int, posicion_ancho:int,posicion_alto:int,eltexto:string)
		tipo="Texto"
		this.x=posicion_x
		this.y=posicion_y
		this.ancho=posicion_ancho
		this.alto=posicion_alto
		texto=eltexto
		escenario.controles.insert (0,this)

	def override pinta(ctx:Cairo.Context)
		
		cuadrado_borde_circular(ctx,x,y,ancho,alto,
			get_rojo(this.colorborde), get_verde(this.colorborde), get_azul(this.colorborde),
			get_rojo(this.colorfondo), get_verde(this.colorfondo), get_azul(this.colorfondo))
		// configurando la letra
		var font_description = new Pango.FontDescription()
		font_description=FontDescription.from_string(this.tipoletra);
		font_description.set_size((int)(this.tamanotexto * Pango.SCALE));
		// configurando la escritura del cuadro usando pango
		var playout = Pango.cairo_create_layout(ctx);
		playout.set_font_description(font_description);
		playout.set_text(texto,4000)
		playout.set_wrap(Pango.WrapMode.WORD_CHAR)
		playout.set_ellipsize(Pango.EllipsizeMode.END)
		ctx.set_source_rgb(get_rojo(this.colorletra), get_verde(this.colorletra), get_azul(this.colorletra))
		ctx.move_to(x+5, y);
		playout.set_width(ancho*Pango.SCALE)
		playout.set_height(alto*Pango.SCALE)
		cairo_show_layout(ctx,playout);
		
		
		
		
	def descale(i:int):int 
		return (i / Pango.SCALE );
				
	
	def override set_tamanotexto(n:int)
		this.tamanotexto=n
	def override set_texto(s:string)
		this.texto=s
		
	def override get_ancho():int
		return this.ancho
		
	def override get_alto():int
		return this.alto
	
	def override get_posx():int
		return this.x
		
	def override get_posy():int
		return this.y
		
	def control_pulsado (c:Control)
		pass
		
	def control_soltado (c:Control)
		pass

	def cuadrado_borde_circular(cr:Cairo.Context,x:int,y:int,ancho:int,alto:int, 
		borde_r:double, borde_g:double, borde_b:double, 
		fondo_r:double,fondo_g:double,fondo_b:double)
		/* a custom shape that could be wrapped in a function */
		var aspect = 1.2;     
		var corner_radius = alto / 10.0;  

		radius:double= corner_radius / aspect;
		degrees:double = 3.14 / 180
		
		
		cr.new_sub_path ();
		cr.arc ( x + ancho - radius, y + radius, radius, -90 * degrees, 0 * degrees);
		cr.arc ( x + ancho - radius, y + alto - radius, radius, 0 * degrees, 90 * degrees);
		cr.arc ( x + radius, y + alto - radius, radius, 90 * degrees, 180 * degrees);
		cr.arc ( x + radius, y + radius, radius, 180 * degrees, 270 * degrees);
		cr.close_path ();

		if this.fondo
			cr.set_source_rgb ( fondo_r,fondo_g,fondo_b);
			cr.fill_preserve ();
		var alpha=1
		if this.borde do alpha=0
		cr.set_source_rgba ( borde_r,borde_g,borde_b,alpha);
		cr.set_line_width ( 1);
		cr.stroke ();
		
